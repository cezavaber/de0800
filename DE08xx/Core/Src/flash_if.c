/*!
** \file flash_if.c
** \author Alessandro Vagniluca
** \date 07/01/2017
** \brief Embedded Flash Interface (without HAL)
** 
** \version 1.0
** \note To be built with no optimization.
**/

#include "main.h"
#include "flash_if.h"


#define FLASH_FLAG_ALLERR	(FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | \
							FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR | FLASH_FLAG_RDERR)

/*!
** \fn HAL_StatusTypeDef _erase_sector( unsigned long Sector )
** \brief RAM function to erase a flash sector in word (32-bit)
** \param Sector FLASH sector to erase
** \return HAL_OK: OK, otherwise: FAILED
** \note This function should be executed from SRAM.
**/ 
//HAL_StatusTypeDef _erase_sector( unsigned long Sector )
HAL_StatusTypeDef __RAM_FUNC _erase_sector( unsigned long Sector )
{
	HAL_StatusTypeDef RetVal = HAL_OK;
	
	// erase the sector in word (32-bit)
	CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
	FLASH->CR |= FLASH_PSIZE_WORD;
	CLEAR_BIT(FLASH->CR, FLASH_CR_SNB);
	FLASH->CR |= FLASH_CR_SER | (Sector << POSITION_VAL(FLASH_CR_SNB));
	FLASH->CR |= FLASH_CR_STRT;

	/*
	Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
	Even if the FLASH operation fails, the BUSY flag will be reset and an error
	flag will be set
	*/
	while( __HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET )
	{
	#ifdef WDOG
		/* Reload IWDG counter with value defined in the reload register */
		__HAL_IWDG_RELOAD_COUNTER(&hiwdg);
	#endif
	}
		
	// check for errors
	if( __HAL_FLASH_GET_FLAG( FLASH_FLAG_ALLERR ) != RESET )
	{
		// error
		RetVal = HAL_ERROR;
	}
        
	/* If the erase operation is completed, disable the SER and SNB Bits */
	CLEAR_BIT(FLASH->CR, (FLASH_CR_SER | FLASH_CR_SNB));
	
	return RetVal;
}

/*!
** \fn HAL_StatusTypeDef _program_wordBuffer( unsigned long Address,
** 											unsigned long *DataBuf,
** 											unsigned long size )
** \brief RAM function to Program a word (32-bit) buffer at a specified address
** \param Address The start address to be programmed
** \param DataBuf The data buffer to be programmed
** \param size    The data word number
** \return HAL_OK: OK, otherwise: FAILED
** \note This function should be executed from SRAM.
**/ 
/*HAL_StatusTypeDef _program_wordBuffer( unsigned long Address,
										unsigned long *DataBuf,
										unsigned long size )*/
HAL_StatusTypeDef __RAM_FUNC _program_wordBuffer( unsigned long Address,
								unsigned long *DataBuf,
								unsigned long size )
{
	HAL_StatusTypeDef RetVal = HAL_OK;
	__IO uint32_t *pd = (__IO uint32_t *)Address;
	
	// program the new data buffer (32-bit)
	CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
	FLASH->CR |= FLASH_PSIZE_WORD;
	FLASH->CR |= FLASH_CR_PG;

	while( size )
	{
		// write the word
		*pd++ = *DataBuf++;

		/*
		Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
		Even if the FLASH operation fails, the BUSY flag will be reset and an error
		flag will be set
		*/
		while( __HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET )
		{
		#ifdef WDOG
			/* Reload IWDG counter with value defined in the reload register */
			__HAL_IWDG_RELOAD_COUNTER(&hiwdg);
		#endif
		}
		
		// check for errors
		if( __HAL_FLASH_GET_FLAG( FLASH_FLAG_ALLERR ) != RESET )
		{
			// abort
			RetVal = HAL_ERROR;
			break;
		}
		
		size--;
	}
    
    /* If the program operation is completed, disable the PG Bit */
    FLASH->CR &= (~FLASH_CR_PG);  
	
	return RetVal;
}


/*!
** \fn void Flash_If_Init( void )
** \brief Initialize Flash interface (unlock + sprotect)
** \return None
**/
void Flash_If_Init( void )
{
	/* Unlock the Flash to enable the flash control register access *************/ 
	if((FLASH->CR & FLASH_CR_LOCK) != RESET)
	{
		/* Authorize the FLASH Registers access */
		FLASH->KEYR = FLASH_KEY1;
		FLASH->KEYR = FLASH_KEY2;
	}
	
	// wait the FLASH operation to complete and clear pending flags
	Wdog();
	while( __HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET );
	Wdog();
	__HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_ALLERR  );
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP);
}

/*!
** \fn void Flash_If_Deinit( void )
** \brief End Flash interface (lock + protect)
** \return None
**/
void Flash_If_Deinit( void )
{
	/* Lock the Flash to disable the flash control register access (recommended
	 to protect the FLASH memory against possible unwanted operation) *********/
	FLASH->CR |= FLASH_CR_LOCK;
}

/*!
** \fn HAL_StatusTypeDef Flash_If_Erase_Sector( unsigned long Sector )
** \brief Erase the specified FLASH memory sector
** \param Sector FLASH sector to erase
** \return HAL_OK: OK, otherwise: FAILED
** \note This function defines the word (32-bit) erase parallelism
** 		(the device voltage range is supposed to be from 2.7V to 3.6V).
**/
HAL_StatusTypeDef Flash_If_Erase_Sector( unsigned long Sector )
{
	HAL_StatusTypeDef RetVal = HAL_ERROR;
	
	if( IS_FLASH_SECTOR(Sector) )
	{
		/* disable all interrupts */
		__disable_irq();
		
		// erase the sector
		RetVal = _erase_sector( Sector );
		
		/*if( __HAL_FLASH_GET_FLAG( FLASH_FLAG_ALLERR ) == RESET )
		{
			RetVal = HAL_OK;
		}*/
		__HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_ALLERR  );
		
		/* Flush the caches to be sure of the data consistency */
		/* Flush instruction cache  */
		if(READ_BIT(FLASH->ACR, FLASH_ACR_ICEN)!= RESET)
		{
			/* Disable instruction cache  */
			__HAL_FLASH_INSTRUCTION_CACHE_DISABLE();
			/* Reset instruction cache */
			__HAL_FLASH_INSTRUCTION_CACHE_RESET();
			/* Enable instruction cache */
			__HAL_FLASH_INSTRUCTION_CACHE_ENABLE();
		}
		/* Flush data cache */
		if(READ_BIT(FLASH->ACR, FLASH_ACR_DCEN) != RESET)
		{
			/* Disable data cache  */
			__HAL_FLASH_DATA_CACHE_DISABLE();
			/* Reset data cache */
			__HAL_FLASH_DATA_CACHE_RESET();
			/* Enable data cache */
			__HAL_FLASH_DATA_CACHE_ENABLE();
		}
		
		/* enable all interrupts */
		__enable_irq();
	}
	
	return RetVal;
}

/*!
** \fn HAL_StatusTypeDef Flash_If_Program_WordBuffer( unsigned long Address,
** 													unsigned long *DataBuf,
** 													unsigned long size )
** \brief Program a word (32-bit) buffer at a specified address
** \param Address The start address to be programmed
** \param DataBuf The data buffer to be programmed
** \param size    The data word number
** \return HAL_OK: OK, otherwise: FAILED
** \note This function must be used when the device voltage range is from 2.7V to 3.6V.
**/
HAL_StatusTypeDef Flash_If_Program_WordBuffer( unsigned long Address,
												unsigned long *DataBuf,
												unsigned long size )
{											
	HAL_StatusTypeDef RetVal = HAL_ERROR;
	
	if( IS_FLASH_ADDRESS(Address) && size )
	{
		/* disable all interrupts */
		__disable_irq();
		
		// program the word buffer
		RetVal = _program_wordBuffer( Address, DataBuf, size );
			
		/*if( __HAL_FLASH_GET_FLAG( FLASH_FLAG_ALLERR ) == RESET )
		{
			RetVal = HAL_OK;
		}*/
		__HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_ALLERR  );
		
		/* enable all interrupts */
		__enable_irq();
	}
	
	return RetVal;
}


