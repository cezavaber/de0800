/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_core.h"
#include "TimeBase.h"
#include "Led.h"
#include "DataFlash.h"
#include "Loader.h"
#include "Host.h"
#include "adc.h"
#include "DecodInput.h"
#include "nvram.h"
#include "rtc.h"
//#include "i2cm_drv.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define T_PULS_ON		(3)	// 3s, minimo tempo pulsante premuto per power-on

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

//#define TEST_DATAFLASH	//!< enable the dataflash erase/write/read test

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

IWDG_HandleTypeDef hiwdg;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;
SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart6;

/* USER CODE BEGIN PV */

/* USB Device Core handle declaration. */
extern USBD_HandleTypeDef hUsbDeviceFS;

/*!
**  \var SystemFlags
**  \brief System flags
**/
TBitDWord SystemFlags;

// valid application mark
//								  01234567
const char sFirma[FIRMA_LEN+1] = "VBR-BOOT";
// ID board
//								0123
const char sIdSk[IDSK_LEN+1] = "BIOS";

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_RTC_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI2_Init(void);
static void MX_I2C1_Init(void);
#ifdef WDOG
static void MX_IWDG_Init(void);
#endif
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

typedef void (*vFunc)( void );

#ifdef TEST_DATAFLASH
/**
  * @brief  Dataflash check.
  * @param  iDFalsh The dataflash index
  * @retval None
  */
void TestDFlash( unsigned char iDFlash )
{
	long i;
	unsigned short len;

	if( iDFlash < NEEBANK )
	{
		strcpy( str, "This is DE080 bootloader");
		len = strlen( str );
		// erase subsector 1 and 2
		if( df_EraseSubSector( iDFlash, 1 ) && df_EraseSubSector( iDFlash, 2 ) )
		{
			// write a buffer in the data-flash on both subsector
			if( df_Write( iDFlash, 2*DF_SUBSECTOR_SIZE-len/2, (unsigned char *)str, len, WR_VER_YES ) )
			{
				// erase subsector 1
				if( !df_EraseSubSector( iDFlash, 1 ) )
				{
				#ifdef LOGGING
					myprintf( "DataFlash #%u sector 1 second erase error\r\n", iDFlash );
				#endif
				}
				// read the buffer from the data-flash subsector 1
				else if( !df_Read( iDFlash, 2*DF_SUBSECTOR_SIZE-len/2, (unsigned char *)str, len/2 ) )
				{
				#ifdef LOGGING
					myprintf( "DataFlash #%u read error\r\n", iDFlash );
				#endif
				}
				else
				{
					// check the erased zone
					for( i=0; i<len/2; i++ )
					{
						if( str[i] != 0xFF )
							break;
					}
					if( i < len/2 )
					{
					#ifdef LOGGING
						myprintf( "DataFlash #%u read data not erased\r\n", iDFlash );
					#endif
					}
					else
					{
					#ifdef LOGGING
						myprintf( "DataFlash #%u test PASSED\r\n", iDFlash );
					#endif
					}
				}
			}
			else
			{
			#ifdef LOGGING
				myprintf( "DataFlash #%u write error\r\n", iDFlash );
			#endif
			}
		}
		else
		{
		#ifdef LOGGING
			myprintf( "DataFlash #%u sector 1 and 2 erase error\r\n", iDFlash );
		#endif
		}
	}
	else
	{
	#ifdef LOGGING
		myprintf( "DataFlash index %u error\r\n", iDFlash );
	#endif
	}
}
#endif

/*!
** \fn short Check4App( void )
** \brief Check if a valid application code is present
** \return 1: present, 0: not present
**/
static short Check4App( void )
{
	short RetVal = 0;
	__IO uint32_t SPvalue = *(__IO uint32_t *)ADDR_APPL;	// app SP value
	__IO uint32_t JumpAddress = *(__IO uint32_t *)(ADDR_APPL + 4);	// app reset address
	
	if( !(SPvalue & 0x3) && // app SP value must have alignment = 4
		//!(JumpAddress & 0x3) &&	// app reset address must have alignment = 4	FALSE!!!!
		(SRAM_START_ADDRESS <= SPvalue) && (SPvalue <= (SRAM_START_ADDRESS+SRAM_SIZE)) &&	// app SP value must be within SRAM
		((ADDR_APPL+4) <= JumpAddress) && (JumpAddress < ADDR_FIRMAAPPL)	// app reset address must be within app flash sectors
		)
	{
		// app must be validated
		if( !memcmp( (const char *)ADDR_FIRMAAPPL, sFirma, FIRMA_LEN ) )
		{
			RetVal = 1;
		}
	}
	
	return RetVal;
}

/*!
** \fn void Jump_To_Application( void )
** \brief Jump to the user application
** \return None
** \note The user application must be previously validated by Check4App()
**/
static void Jump_To_Application( void )
{
	__IO uint32_t SPvalue = *(__IO uint32_t *)ADDR_APPL;	// app SP value
	__IO uint32_t JumpAddress = *(__IO uint32_t *)(ADDR_APPL + 4);	// app reset address
	vFunc AppEntry = (vFunc)JumpAddress;		// app reset vector
	
	// power off dataflash
	HAL_GPIO_WritePin( EN_FLASH_GPIO_Port, EN_FLASH_Pin, GPIO_PIN_SET );
	// power off VBATT OPA
	HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
	// Radio-modem RCQ2-868 in low-power
	HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_SET);	// RADIO reset off
	HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
	HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_RESET);	// RADIO power-down on
	// FT801 in low-power
	HAL_GPIO_WritePin(PD_FT801_GPIO_Port, PD_FT801_Pin, GPIO_PIN_RESET);
	// buzzer off
	BSP_BUZ_OnOff( BUZ_OFF );
	// switch-off all external peripherals
	HAL_GPIO_WritePin( EN_PWR_GPIO_Port, EN_PWR_Pin, GPIO_PIN_SET );

	Wdog();
	
	/* stop and deinit all the used peripherals */
	if( hUsbDeviceFS.dev_state )
	{
		USBD_DeInit( &hUsbDeviceFS );
	}
	HAL_UART_DeInit( &huart6 );
	HAL_UART_DeInit( &huart2 );
	HAL_I2C_DeInit( &hi2c1 );
	HAL_SPI_DeInit( &hspi1 );
	HAL_SPI_DeInit( &hspi2 );
	//HAL_RTC_DeInit( &hrtc );
	HAL_ADC_DeInit( &hadc1 );
	/* DMA controller clock disable */
	__HAL_RCC_DMA2_CLK_DISABLE();

	/* GPIO Ports Clock Disable */
	__HAL_RCC_GPIOC_CLK_DISABLE();
	__HAL_RCC_GPIOH_CLK_DISABLE();
	__HAL_RCC_GPIOA_CLK_DISABLE();
	__HAL_RCC_GPIOB_CLK_DISABLE();
	__HAL_RCC_GPIOD_CLK_DISABLE();
	
	Wdog();
	
	/* RCC_IRQn interrupt disable */
	HAL_NVIC_DisableIRQ(RCC_IRQn);
	HAL_RCC_DeInit();	// disable also all interrupts

	/* Disable SysTick Interrupt */
	HAL_SuspendTick();
	HAL_DeInit();	// stops also the systick
	
	Wdog();
	
	/* disable all interrupts */
	__disable_irq();
	
	/* Initialize user application's Stack Pointer */
	__set_MSP( SPvalue );
	// jump to the user application
	AppEntry();
	
	/*!
	 * ATTENZIONE
	 * Il watchdog, se abilitato, rimane in funzione.
	 * L'applicativo, in questo caso, deve gestirlo.
	 *
	 * Tutti gli interrupt delle periferiche devono essere disabilitati:
	 * 	NVIC->ISER0 = 0
	 * 	NVIC->ISER1 = 0
	 * 	NVIC->ISER2 = 0
	 * altrimenti si rischia che si attivi il Default_Handler nell'applicativo.
	 */

	while( 1 );
}

/*!
** \fn void EnableJumpToApplication( void )
** \brief Enable an immediate jump to the user application
** \return None
**
static void EnableJumpToApplication( void )
{
	// force a bootloader timeout
	starttimer( TRUN, 0 );
	while( checktimer( TRUN ) == INCORSO )
	{
		Wdog();
		led_Handler();
	}
}

/*!
** \fn short Wait4JumpToApplication( void )
** \brief Wait for jumping to the user application
** \return 1: ok jump, 0: do not jump
**/ 
static short Wait4JumpToApplication( void )
{
	short RetVal = 0;
	unsigned short timeout = T_PULS_ON;	// (s) minimo tempo pulsante premuto per power-on
	unsigned short i;

	// wait with pressed pushbutton
	RTC_TIME = 0;
	for( i=0; i<timeout; )
	{
		Wdog();
		led_Handler();
		inpHandler();	// ADC attivato ogni 5ms
		if( !DIGIN_PULS )
		{
			// pushbutton not pressed
			break;
		}

		if( RTC_TIME )
		{
			// procedure ogni 1s
			RTC_TIME = 0;
			i++;

			rtcRd( NULL );
		}

		HAL_Delay( 1 );
	}
	inpMonitor();
	if( i >= timeout )
	{
		RetVal = 1;
	}

	return RetVal;
}

/*!
** \fn void SystemReset( void )
** \brief System reset
** \return None
**/
void SystemReset( void )
{
	/* disable all interrupts */
	__disable_irq();
	/* System Reset */
	HAL_NVIC_SystemReset();
	// ------> NON PASSA MAI DI QUI
}

/**
  * @brief  This function configures the system to enter Standby mode with RTC
  *         clocked by LSE or LSI and with Backup SRAM ON.
  *         STANDBY Mode with RTC clocked by LSE/LSI and BKPSRAM
  *         ====================================================
  *           - RTC Clocked by LSE or LSI
  *           - Backup SRAM ON
  * @param  None
  * @retval None
  */
void StandbyRTCBKPSRAMMode(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	unsigned long NSec = 0;

	if( hUsbDeviceFS.dev_state )
	{
		USBD_DeInit( &hUsbDeviceFS );
	}

	/* disable all interrupts */
	__disable_irq();

	/* Disable SysTick Interrupt */
	HAL_SuspendTick();

	// disable all EXTI interrupts
	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	// also disable INT_FT801, if used

	Wdog();

	// power off dataflash
	HAL_GPIO_WritePin( EN_FLASH_GPIO_Port, EN_FLASH_Pin, GPIO_PIN_SET );
	// power off VBATT OPA
	HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
	// Radio-modem RCQ2-868 in low-power
	HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_SET);	// RADIO reset off
	HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
	HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_RESET);	// RADIO power-down on
	// FT801 in low-power
	HAL_GPIO_WritePin(PD_FT801_GPIO_Port, PD_FT801_Pin, GPIO_PIN_RESET);
	// buzzer off
	BSP_BUZ_OnOff( BUZ_OFF );
	// switch-off all external peripherals
	HAL_GPIO_WritePin( EN_PWR_GPIO_Port, EN_PWR_Pin, GPIO_PIN_SET );

	Wdog();

	/* stop and deinit all the used peripherals */
	HAL_UART_DeInit( &huart2 );
	HAL_I2C_DeInit( &hi2c1 );
	HAL_SPI_DeInit( &hspi1 );
	HAL_SPI_DeInit( &hspi2 );
	//HAL_RTC_DeInit( &hrtc );
	HAL_ADC_DeInit( &hadc1 );
	/* DMA controller clock disable */
	__HAL_RCC_DMA2_CLK_DISABLE();

	// To enter in STANDBY mode all the wake-up sources must be low
	if( DIGIN_PULS || DIGIN_ALIM )
	{
	#ifdef LOGGING
		if( DIGIN_PULS )
		{
			myprintf("PUSHB is pressed\r\n");
		}
		if( DIGIN_ALIM )
		{
			myprintf("SUPPLY is present\r\n");
		}
		myprintf("STOP Mode cycles: ");
	#endif
		if( !DIGIN_PULS )
		{
			NSec = 0;
		}

		// Configure all GPIO as analog to reduce current consumption on non used IOs
		/* GPIO Ports Clock Enable */
		__HAL_RCC_GPIOC_CLK_ENABLE();
		__HAL_RCC_GPIOH_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();
		__HAL_RCC_GPIOD_CLK_ENABLE();
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Pin = GPIO_PIN_All;
		HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
		HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	#ifdef LOGGING
		GPIO_InitStruct.Pin = GPIO_PIN_All & (~GPIO_PIN_6);	// keep active USART6_TX pin on GPIOC
	#endif
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
		GPIO_InitStruct.Pin = GPIO_PIN_All & (~(PRES_RETE_Pin | PULS_Pin));	// keep active PRES_RETE_Pin and PULS_Pin pins on GPIOB
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
		/* GPIO Ports Clock Disable */
		__HAL_RCC_GPIOD_CLK_DISABLE();
		__HAL_RCC_GPIOH_CLK_DISABLE();
		__HAL_RCC_GPIOA_CLK_DISABLE();
	#ifndef LOGGING
		__HAL_RCC_GPIOC_CLK_DISABLE();
	#endif
		//__HAL_RCC_GPIOB_CLK_DISABLE();
		
		// the RTC wake-up timer interrupt must not be disabled
		__HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);
		HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
		/* enable all interrupts */
		__enable_irq();

		// enter in STOP mode for 1s and then check all the wake-up sources
		do
		{
			Wdog();
			PULS_INP = DIGIN_PULS;
			ALIM_INP = DIGIN_ALIM;
		#ifdef LOGGING
			outbyte( 'I' );
			HAL_UART_DeInit( &huart6 );
		#endif

			// Wake-up timer already enabled every 1s

			/* FLASH Deep Power Down Mode enabled */
			HAL_PWREx_EnableFlashPowerDown();

			/*## Enter Stop Mode #######################################################*/
			HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

			/*## Exit from Stop Mode #######################################################*/
			Wdog();
			//SystemClock_Config();
		#ifdef LOGGING
			MX_USART6_UART_Init();	// HSI = system clock => bit rate is set to 19200
			outbyte( 'O' );
		#endif
			// check all the wake-up sources
			if( HAL_GPIO_ReadPin( PULS_GPIO_Port, PULS_Pin ) == GPIO_PIN_SET )
			{
				DIGIN_PULS = 1;
				NSec++;
			}
			else
			{
				DIGIN_PULS = 0;
				NSec = 0;
			}
			if( HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET )
			{
				DIGIN_ALIM = 1;
			}
			else
			{
				DIGIN_ALIM = 0;
			}
			if( //(!PULS_INP && DIGIN_PULS)
				(NSec >= T_PULS_ON)	// PUSHB is pressed for the minimum T_PULS_ON time
				|| (!ALIM_INP && DIGIN_ALIM)	// SUPPLY was connected
				)
			{
				// simulate a wake-up event: restart
			#ifdef LOGGING
				outbyte( 'W' );
			#endif
				SystemReset();
			}

		} while( DIGIN_PULS || DIGIN_ALIM );
	}

#ifdef LOGGING
	myprintf("STANDBY Mode entry...\r\n");
#endif
	HAL_UART_DeInit( &huart6 );
	/* GPIO Ports Clock Disable */
	__HAL_RCC_GPIOC_CLK_DISABLE();
	__HAL_RCC_GPIOH_CLK_DISABLE();
	__HAL_RCC_GPIOA_CLK_DISABLE();
	__HAL_RCC_GPIOB_CLK_DISABLE();
	__HAL_RCC_GPIOD_CLK_DISABLE();

	/* disable all interrupts */
	__disable_irq();

	Wdog();
	
	/* Disable Wake-up timer */
	HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);	// Interrupt disable
	HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);

	/*## Clear all related wakeup flags ########################################*/

	/* Disable all used wakeup sources: Pin1(PA.0) */
	HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
	/* Clear PWR wake up Flag */
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

	/* Clear RTC Wake Up timer Flag */
	__HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);

	/* Enable the Backup SRAM low power Regulator */
	HAL_PWREx_EnableBkUpReg();

	/* Re-enable all used wakeup sources: Pin1(PA.0) */
	HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);

	/*## Enter Standby Mode ####################################################*/
	/* Request to enter STANDBY mode  */
	HAL_PWR_EnterSTANDBYMode();
	// ------> NON PASSA MAI DI QUI
	
	/*!
	 * ATTENZIONE
	 * Il watchdog, se abilitato, rimane in funzione.
	 * In questo caso la cpu si risveglia per il reset da watchdog.
	 * Il bootloader all'avvio deve riconoscere il reset da watchdog avvenuto in
	 * Standby mode e ritornare subito in Standby mode senza abilitare il 
	 * watchdog.
	 */
	
	SystemReset();	// se fallisse l'ingresso in Standby mode...
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	int i;
	unsigned long regPWR_CSR;
	unsigned long regRCC_CR;
	unsigned long regRCC_BDCR;
	unsigned long regRCC_CSR;
	unsigned long regRCC_CIR;
	
	FLAGS = 0;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

	/* Enable Power Clock */
	__HAL_RCC_PWR_CLK_ENABLE();

	// read System and Reset flags
	regPWR_CSR = PWR->CSR;
	regRCC_CR = RCC->CR;
	regRCC_BDCR = RCC->BDCR;
	regRCC_CSR = RCC->CSR;
	regRCC_CIR = RCC->CIR;

	/* Check and handle if the system was resumed from Standby mode */
	if(__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET)
	{
		WAS_STANDBY = 1;	// ero in Standby
		__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
	}
	if(__HAL_PWR_GET_FLAG(PWR_FLAG_WU) != RESET)
	{
		WAS_WAKEUP = 1;	// avvenuto wakeup
		__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
	}
	/* Check if Independent Watchdog Reset flag is set */
	if(__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET)
	{
		WAS_IWDG = 1;	// avvenuto un watchdog reset

		if( WAS_STANDBY && !WAS_WAKEUP )
		{
			// ero in STANDBY -> ritorno in STANDBY senza abilitare il watchdog
			/* Clear source Reset Flag */
			__HAL_RCC_SYSCFG_CLK_ENABLE();
			__HAL_RCC_CLEAR_RESET_FLAGS();
		    /*## Clear all related wakeup flags ########################################*/

		    /* Disable all used wakeup sources: Pin1(PA.0) */
		    HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
		    /* Clear PWR wake up Flag */
		    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

		    /* Enable the Backup SRAM low power Regulator */
		    HAL_PWREx_EnableBkUpReg();

		    /* Re-enable all used wakeup sources: Pin1(PA.0) */
		    HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);
		#ifdef EN_STANDBY
		    /*## Enter Standby Mode ####################################################*/
		    /* Request to enter STANDBY mode with RTC clocked by LSE/LSI and BKPSRAM */
		    HAL_PWR_EnterSTANDBYMode();
		#else
			SystemReset();
		#endif
			// ------> NON PASSA MAI DI QUI
		}
	}

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
#ifdef WDOG
  MX_IWDG_Init();
#endif
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_RTC_Init();
  MX_USART6_UART_Init();
  MX_USART2_UART_Init();
  MX_ADC1_Init();
  MX_SPI1_Init( CLK_SLOW );
  MX_SPI2_Init();
  //MX_I2C1_Init();	// da fare con il bus I2C alimentato
  //MX_USB_DEVICE_Init();	ALTRIMENTI SI CAUSA UN RESET (ASSORBIMENTO) QUANDO SI ATTIVA IL +3V !!!
  /* USER CODE BEGIN 2 */

	Wdog();

#ifdef LOGGING
	myprintf("\r\n\n%s\r\n\n", sFW_VERSIONE);
	myprintf("PWR_CSR  = 0x%08X\r\n", regPWR_CSR);
	myprintf("RCC_CR   = 0x%08X\r\n", regRCC_CR);
	myprintf("RCC_BDCR = 0x%08X\r\n", regRCC_BDCR);
	myprintf("RCC_CSR  = 0x%08X\r\n", regRCC_CSR);
	myprintf("RCC_CIR  = 0x%08X\r\n\n", regRCC_CIR);
	if( WAS_STANDBY )
	{
		myprintf("Exit from STANDBY\r\n");
	}
	if( WAS_WAKEUP )
	{
		myprintf("Wake-up occurred\r\n");
	}
	if( WAS_IWDG )
	{
		myprintf("Independent Watchdog Reset occurred\r\n");
	}
#endif
	/* Clear source Reset Flag */
	__HAL_RCC_CLEAR_RESET_FLAGS();

	if( !rtcInit() )
	{
	#ifdef LOGGING
		myprintf("RTC-NVRAM default\r\n");
	#endif
	}
#ifdef LOGGING
	ScriviData( str );
	ScriviOra( &str[12] );
	myprintf("Data-Ora: %s %s\r\n", str, &str[12]);
#endif

	DWT_Delay_Init();
	led_Init();
	led_Set( BUZ, LEDSTAT_MANUAL );
	BSP_BUZ_OnOff( BUZ_OFF );

	// switch-on all external peripherals
	HAL_GPIO_WritePin( EN_PWR_GPIO_Port, EN_PWR_Pin, GPIO_PIN_RESET );
	HAL_Delay( 100 );
	// FT801 in low-power
	HAL_GPIO_WritePin(PD_FT801_GPIO_Port, PD_FT801_Pin, GPIO_PIN_RESET);
	// Radio-modem RCQ2-868 in low-power
	HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_SET);	// RADIO reset off
	HAL_GPIO_WritePin(CONF_RF_GPIO_Port, CONF_RF_Pin, GPIO_PIN_SET);	// disable RADIO config
	HAL_GPIO_WritePin(PD_RF_GPIO_Port, PD_RF_Pin, GPIO_PIN_RESET);	// RADIO power-down on


#ifdef TEST_DATAFLASH
	// check data-flash bank #0
	if( !df_Init( EE_BANK_0 ) )
	{
	#ifdef LOGGING
		myprintf("DATAFLASH #0 Init error\r\n");
	#endif
	}
	else
	{
		TestDFlash( EE_BANK_0 );
	}
	df_Deinit( EE_BANK_0 );
#endif

	// init inputs and I2C slaves
	MX_I2C1_Init();
	//i2cInit();
	inpInit();

	// initial inputs settle time 
	for( i=0; i<1200; i++ )	// prima misura VBatt valida dopo i primi 1000ms
	{
		Wdog();
		led_Handler();
		inpHandler();	// ADC attivato ogni 5ms

		if( RTC_TIME )
		{
			// procedure ogni 1s
			RTC_TIME = 0;

			rtcRd( NULL );
		}
		
		HAL_Delay( 1 );
	}
	inpMonitor();
	
	// check if a valid application code is present
	if( Check4App() )
	{
		APP_NOT_PRESENT = 0;
	}
	else
	{
		APP_NOT_PRESENT = 1;
	}
	
	// check if an external supply is present
	if( !DIGIN_ALIM )
	{
		// no external supply
	#ifdef LOGGING
		myprintf("No external supply\r\n");
	#endif
		if( !APP_NOT_PRESENT )
		{
			// check for enough VBATT level
			if( VBatt >= VBatt_tab.VBattLow )
			{
				/* Jump to user application */
				if( WAS_STANDBY && WAS_WAKEUP )
				{
					/* Ero in STANDBY e sono stato risvegliato sicuramente da pulsante
					 * ==> Lancio applicativo solo se pulsante premuto per almeno
					 * il tempo minimo.
					 */
				#ifdef LOGGING
					myprintf( "Check PUSHB pressed...\r\n" );
				#endif
					if( Wait4JumpToApplication()
						&& (VBatt >= VBatt_tab.VBattLow)
						)
					{
					#ifdef LOGGING
						myprintf( "Jump to the User App\r\n" );
					#endif
						Jump_To_Application();
					}
				}
				else
				{
				#ifdef LOGGING
					myprintf( "Jump to the User App\r\n" );
				#endif
					Jump_To_Application();
				}
			}
			else
			{
			#ifdef LOGGING
				myprintf("Not enough VBATT\r\n");
			#endif
				// buzzer 3 bips
				for( i=0; i<3; i++ )
				{
					Wdog();
					led_Handler();
					inpHandler();
					BSP_BUZ_OnOff( BUZ_ON );
					HAL_Delay( 100 );
					led_Handler();
					inpHandler();
					BSP_BUZ_OnOff( BUZ_OFF );
					HAL_Delay( 100 );
				}
			}
		}
		else
		{
		#ifdef LOGGING
			myprintf("No User App\r\n");
		#endif
			// buzzer 2 bips
			for( i=0; i<2; i++ )
			{
				Wdog();
				led_Handler();
				inpHandler();
				BSP_BUZ_OnOff( BUZ_ON );
				HAL_Delay( 100 );
				led_Handler();
				inpHandler();
				BSP_BUZ_OnOff( BUZ_OFF );
				HAL_Delay( 100 );
			}
		}
	#ifdef LOGGING
		myprintf("Power off...\r\n");
	#endif
		Wdog();
	#ifdef EN_STANDBY
		StandbyRTCBKPSRAMMode();
	#else
		SystemReset();
	#endif
		// ------> NON PASSA MAI DI QUI
	}
	else if( !ENABLE_BOOT )
	{
		// an external supply is present, but the bootloader was not enabled
	#ifdef LOGGING
		myprintf("External supply is present, bootloader was not enabled\r\n");
	#endif
		if( !APP_NOT_PRESENT )
		{
			/* Jump to user application */
		#ifdef LOGGING
			myprintf( "Jump to the User App\r\n" );
		#endif
			Jump_To_Application();
		}
		else
		{
		#ifdef LOGGING
			myprintf("No User App\r\n");
		#endif
			// start bootloader
		}
	}
	else
	{
		// an external supply is present and the bootloader was enabled
	#ifdef LOGGING
		myprintf("External supply is present, bootloader was enabled\r\n");
	#endif
		// clear bootloader enable flag
		ENABLE_BOOT = 0;
		// update NVRAM
		nvramUpdate();

		// start bootloader
	}
	
#ifdef LOGGING
	myprintf("\r\nSTART BOOTLOADER\r\n\n");
#endif
	MX_USB_DEVICE_Init();

	// buzzer long bip (2s)
	BSP_BUZ_OnOff( BUZ_ON );
	for( i=0; i<2000; i++ )
	{
		Wdog();
		led_Handler();
		inpHandler();
		HAL_Delay( 1 );
	}
	BSP_BUZ_OnOff( BUZ_OFF );
	
	// wait for pushbutton unpressed
	while( DIGIN_PULS )
	{
		Wdog();
		led_Handler();
		inpHandler();
	}
	PULS_INP = 0;

	rtcRd( NULL );
	RTC_TIME = 0;
	inpMonitor();
	
	starttimer( THOST, HOST_TIMEOUT );
	
	if( Host_IsEnabled() )
	{
	#ifdef LOGGING
		myprintf( "PC enabled\r\n" );
	#endif
		Host_Init();	// init serial bootloader
	}
	
	
	// start bootloader timeout
	starttimer( TRUN, BOOT_TIMEOUT );

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

		Wdog();
		led_Handler();
		inpHandler();

		if( RTC_TIME )
		{
			// procedure ogni 1s
			RTC_TIME = 0;

			rtcRd( NULL );
		}

		/* serial bootloader handler */
		if( Host_IsEnabled() )
		{
			starttimer( TRUN, BOOT_TIMEOUT );
			if( !HOST_RUN )
			{
			#ifdef LOGGING
				myprintf( "PC enabled\r\n" );
			#endif
				Host_Init();	// init serial bootloader
			}
			else
			{
				i = Host_Handler();	// run serial bootloader
				if( i == HOST_RET_END_OK )
				{
					/* // force an immediate jump to the application
					EnableJumpToApplication(); */
					// check if a valid application code is present
					if( Check4App() )
					{
						APP_NOT_PRESENT = 0;
					}
					else
					{
						APP_NOT_PRESENT = 1;
					}
				}
			}
		}
		else if( HOST_RUN )
		{
		#ifdef LOGGING
			myprintf( "PC disabled\r\n" );
		#endif
			Host_Deinit();	// deinit serial bootloader
		}
		
		// power-off from user?
		if( DIGIN_PULS )
		{
			starttimer( TRUN, BOOT_TIMEOUT );
			if( !PULS_INP )
			{
			#ifdef LOGGING
				myprintf("Pushbutton is pressed\r\n");
			#endif
				PULS_INP = 1;
				starttimer( T_PULS, 3*T1SEC );
			}
			else if( checktimer( T_PULS ) != INCORSO )
			{
				Host_Deinit();	// deinit serial bootloader
				// buzzer 2 bips
				for( i=0; i<2; i++ )
				{
					Wdog();
					led_Handler();
					inpHandler();
					BSP_BUZ_OnOff( BUZ_ON );
					HAL_Delay( 100 );
					led_Handler();
					inpHandler();
					BSP_BUZ_OnOff( BUZ_OFF );
					HAL_Delay( 100 );
				}
			#ifdef LOGGING
				myprintf("User power off...\r\n");
			#endif

			#ifdef EN_STANDBY
				StandbyRTCBKPSRAMMode();
			#else
				SystemReset();
			#endif
				// ------> NON PASSA MAI DI QUI
			}
		}
		else
		{
		#ifdef LOGGING
			if( PULS_INP )
			{
				myprintf("Pushbutton released\r\n");
				inpMonitor();
			}
		#endif
			PULS_INP = 0;
		}

		if( checktimer( TRUN ) != INCORSO )
		{
			// bootloader timeout
			Host_Deinit();	// deinit serial bootloader
			
			/* check if a valid application code is present */
			if( Check4App() )
			{
				APP_NOT_PRESENT = 0;
				if( !DIGIN_ALIM )
				{
					// an external supply is no more present
					// check for enough VBATT level
					if( VBatt < VBatt_tab.VBattLow )
					{
					#ifdef LOGGING
						myprintf("Not enough VBATT\r\n");
					#endif
						// buzzer 3 bips
						for( i=0; i<3; i++ )
						{
							Wdog();
							led_Handler();
							inpHandler();
							BSP_BUZ_OnOff( BUZ_ON );
							HAL_Delay( 100 );
							led_Handler();
							inpHandler();
							BSP_BUZ_OnOff( BUZ_OFF );
							HAL_Delay( 100 );
						}
					#ifdef LOGGING
						myprintf("Power off...\r\n");
					#endif
						Wdog();
					#ifdef EN_STANDBY
						StandbyRTCBKPSRAMMode();
					#else
						SystemReset();
					#endif
						// ------> NON PASSA MAI DI QUI
					}
				}
				/* Jump to user application */
			#ifdef LOGGING
				myprintf( "Jump to the User App\r\n" );
			#endif
				Jump_To_Application();
			}
			else
			{
				APP_NOT_PRESENT = 1;
			#ifdef LOGGING
				myprintf("No User App\r\n");
			#endif
				if( DIGIN_ALIM )
				{
					// an external supply is still present: remain in bootloader mode
				#ifdef LOGGING
					myprintf("Bootloader keeps running\r\n");
				#endif
					starttimer( TRUN, BOOT_TIMEOUT );
				}
				else
				{
					// buzzer 2 bips
					for( i=0; i<2; i++ )
					{
						Wdog();
						led_Handler();
						inpHandler();
						BSP_BUZ_OnOff( BUZ_ON );
						HAL_Delay( 100 );
						led_Handler();
						inpHandler();
						BSP_BUZ_OnOff( BUZ_OFF );
						HAL_Delay( 100 );
					}
				#ifdef LOGGING
					myprintf("Power off...\r\n");
				#endif
					Wdog();
				#ifdef EN_STANDBY
					StandbyRTCBKPSRAMMode();
				#else
					SystemReset();
				#endif
					// ------> NON PASSA MAI DI QUI
				}
			}
		}

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE
                              |RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_112CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  sConfig.Rank = 2;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

#ifdef WDOG
/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_16;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}
#endif

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  //RTC_TimeTypeDef sTime = {0};
  //RTC_DateTypeDef sDate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /**Initialize RTC Only 
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
    
  /* USER CODE END Check_RTC_BKUP */

  /**Initialize RTC and set the Time and Date 
  *
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_TUESDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x19;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  } */

  /**Enable the WakeUp 
  */
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 2048, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/*!
** \fn void MX_SPI1_Init(unsigned char mode)
** \brief SPI1 init function
** \param mode The SPI clock configuration mode (CLK_SLOW or CLK_FAST)
** \return None
**/
void MX_SPI1_Init(unsigned char mode)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
	if( mode == CLK_FAST )
	{
		hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
	}
	else
	{
		hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	}
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, SPI1_NSS_Pin|USART2_RTS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, PD_FT801_Pin|EN_FLASH_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, SPI2_NSS_Pin|CONF_RF_Pin|EN_PWR_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, BUZZER_Pin|PD_RF_Pin|EN_V_BATT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RESET_RF_GPIO_Port, RESET_RF_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(EN_V_BCKL_GPIO_Port, EN_V_BCKL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CHARGE_STAT_Pin */
  GPIO_InitStruct.Pin = CHARGE_STAT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(CHARGE_STAT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PC0 PC10 PC11 PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : INT1_ACC_Pin INT2_ACC_Pin */
  GPIO_InitStruct.Pin = INT1_ACC_Pin|INT2_ACC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : USART2_CTS_Pin */
  GPIO_InitStruct.Pin = USART2_CTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(USART2_CTS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI1_NSS_Pin USART2_RTS_Pin EN_V_BCKL_Pin */
  GPIO_InitStruct.Pin = SPI1_NSS_Pin|USART2_RTS_Pin|EN_V_BCKL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PD_FT801_Pin EN_FLASH_Pin RESET_RF_Pin */
  GPIO_InitStruct.Pin = PD_FT801_Pin|EN_FLASH_Pin|RESET_RF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : INT_FT801_Pin BOOT1_Pin PRES_RETE_Pin PULS_Pin */
  GPIO_InitStruct.Pin = INT_FT801_Pin|BOOT1_Pin|PRES_RETE_Pin|PULS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : SPI2_NSS_Pin BUZZER_Pin CONF_RF_Pin PD_RF_Pin 
                           EN_V_BATT_Pin EN_PWR_Pin */
  GPIO_InitStruct.Pin = SPI2_NSS_Pin|BUZZER_Pin|CONF_RF_Pin|PD_RF_Pin 
                          |EN_V_BATT_Pin|EN_PWR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PA9 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PD2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  //HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  //HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

#ifdef LOGGING

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
int outbyte( int c )
{
  /* Place your implementation of fputc here *
  Wdog();
#if defined( LOGGING_UART6 )
  HAL_UART_Transmit(&huart6, (uint8_t *)&c, 1, 1000);
#endif
  Wdog(); */
	debug_out(c);

  return c;
}

/*!
**  \fn short LOCKprintf( void )
**  \brief Lock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short LOCKprintf( void )
{
	short RetVal = 0;

	/* if( osKernelRunning() )
	{
		if( osMutexWait(printfMutexHandle, osWaitForever) == osOK )
		{
			RetVal = 1;
		}
	} */

	return RetVal;
}

/*!
**  \fn short UNLOCKprintf( void )
**  \brief Unlock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short UNLOCKprintf( void )
{
	short RetVal = 0;

	/* if( osKernelRunning() )
	{
		if( osMutexRelease(printfMutexHandle) == osOK )
		{
			RetVal = 1;
		}
	} */

	return RetVal;
}

#endif	// LOGGING

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
#ifdef LOGGING
	myprintf( "HAL Error\r\n" );
#endif
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
#ifdef LOGGING
	myprintf( "Wrong parameters value: file %s on line %d\r\n", file, line );
#endif
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
