/*!
** \file Loader.c
** \author Alessandro Vagniluca
** \date 15/12/2016
** \brief Code/Data loader handler (Motorola S-record)
** 
** \version 1.0
**/

#define ENAB_FLASH_WRITE	// enable erase/write flash
#ifdef ENAB_FLASH_WRITE
	#define ENAB_BLANK_CHECK	// enable blanck-check on the erased flash sector
#endif
#define ENAB_VERIFY		// enable data verify

#include "Loader.h"
//#include "eeprom.h"
#include "Utils.h"
#include "flash_if.h"
#include "DataFlash.h"


#define S1RECORD_MINLEN		(1+1+2+4+2+2)	// 12 minimum S1 ascii data record length
#define S2RECORD_MINLEN		(1+1+2+6+2+2)	// 14 minimum S2 ascii data record length
#define S3RECORD_MINLEN		(1+1+2+8+2+2)	// 16 minimum S3 ascii data record length
#define SRECORD_MAXLEN		(1+1+2+8+2*MAXDATASIZE)	// 524 maximum ascii S-record length

#define FLASH_NUMSECT		(FLASH_SECTOR_TOTAL)	//!< the total number of flash sectors

/*!
** \var FlashSector_tab
** \brief Flash sectors table
**/
typedef struct
{
	//unsigned char SectorID;
	unsigned long StartAddress;
	unsigned long EndAddress;
} SECT_STRUCT;
static const SECT_STRUCT FlashSector_tab[FLASH_NUMSECT] =
{
	{ /*FLASH_SECTOR_0,*/ ADDR_FLASH_SECTOR_0, ADDR_FLASH_SECTOR_1-1 },	//!< boot
	{ /*FLASH_SECTOR_1,*/ ADDR_FLASH_SECTOR_1, ADDR_FLASH_SECTOR_2-1 },	//!< boot
	{ /*FLASH_SECTOR_2,*/ ADDR_FLASH_SECTOR_2, ADDR_FLASH_SECTOR_3-1 },	//!< boot
	{ /*FLASH_SECTOR_3,*/ ADDR_FLASH_SECTOR_3, ADDR_FLASH_SECTOR_4-1 },	//!< boot
	{ /*FLASH_SECTOR_4,*/ ADDR_FLASH_SECTOR_4, ADDR_FLASH_SECTOR_5-1 },	//!< app
	{ /*FLASH_SECTOR_5,*/ ADDR_FLASH_SECTOR_5, ADDR_FLASH_SECTOR_6-1 },	//!< app
	{ /*FLASH_SECTOR_6,*/ ADDR_FLASH_SECTOR_6, ADDR_FLASH_SECTOR_7-1 },	//!< app
	{ /*FLASH_SECTOR_7,*/ ADDR_FLASH_SECTOR_7, ADDR_FLASH_END-1 },		//!< app
};

/*!
** \def BOOT_LAST_SECTOR
** \brief The last flash sector assigned to the bootloader
**/
#define BOOT_LAST_SECTOR			(FLASH_SECTOR_3)
/*!
** \def APP_FIRST_SECTOR
** \brief The first flash sector assigned to the application
**/
#define APP_FIRST_SECTOR			(FLASH_SECTOR_4)

#define FIRST_APPL_SECTOR_INDEX			4
#define SECOND_APPL_SECTOR_INDEX		5
#define THIRD_APPL_SECTOR_INDEX			6
#define FOURTH_APPL_SECTOR_INDEX		7

// flash sector erased flags (1 = erased)
static unsigned char FlashSectErased[FLASH_NUMSECT/8+1];

// dataflash subsector erased flags (1 = erased)
static unsigned char DataFlashSubSectErased[NUM_SUBSECT_CHIP/8+1];

/*!
** \var Data
** \brief The data buffer
**/
DATASTRUCT *Data = NULL;


/**
  * @brief  Gets the sector of a given address
  * @param  None
  * @retval The sector of a given address
  *
static uint32_t GetSector(uint32_t Address)
{
  uint32_t sector = 0;
  
  if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))
  {
    sector = FLASH_SECTOR_0;  
  }
  else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))
  {
    sector = FLASH_SECTOR_1;  
  }
  else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))
  {
    sector = FLASH_SECTOR_2;  
  }
  else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))
  {
    sector = FLASH_SECTOR_3;  
  }
  else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))
  {
    sector = FLASH_SECTOR_4;  
  }
   else	//(Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_5))
  {
    sector = FLASH_SECTOR_5;
  }

  return sector;
}

/**
  * @brief  Gets sector Size
  * @param  None
  * @retval The size of a given sector
  *
static uint32_t GetSectorSize(uint32_t Sector)
{
  uint32_t sectorsize = 0x00;

  if((Sector == FLASH_SECTOR_0) || (Sector == FLASH_SECTOR_1) || (Sector == FLASH_SECTOR_2) || (Sector == FLASH_SECTOR_3))
  {
    sectorsize = 16 * 1024;
  }
  else if(Sector == FLASH_SECTOR_4)
  {
    sectorsize = 64 * 1024;
  }
  else
  {
    sectorsize = 128 * 1024;
  }  
  return sectorsize;
}

/***************************************************************************
* FUNZIONE  : short CheckDigit( const char *s )
* SCOPO     : Verifica il range dei caratteri del record
* ARGOMENTI : s - stringa contenente il record (senza CR e LF)
* RETURN    : 1:OK, 0:errore
***************************************************************************/
static short CheckDigit( const char *s )
{
	short RetVal = 0;

	while( *s )
	{
		if( (*s < '0') || (*s > 'F') )
			break;
		else if( (*s > '9') && (*s < 'A') )
			break;
		s++;
		if( *s == '\0' )
			RetVal = 1;
	}

	return RetVal;
}


/*!
** \fn short loader_Init( unsigned char TargetMemory )
** \brief Initialize the loader on the target memory
** \param TargetMemory  The target memory
** \return 1: OK, 0: FAILED
**/
short loader_Init( unsigned char TargetMemory )
{
	short RetVal = 1;
	
	switch( TargetMemory )
	{
		case MEM_ROMCODE:
		case MEM_ROMDATA:
			memset( FlashSectErased, 0, sizeof(FlashSectErased) );
		case MEM_DFDATA1:
			df_Init( EE_BANK_0 );
		/* case MEM_DFDATA2:
			df_Init( EE_BANK_1 ); */
			memset( DataFlashSubSectErased, 0, sizeof(DataFlashSubSectErased) );
		// case MEM_EEDATA1:
		// case MEM_EEDATA2:
			if( Data == NULL )
			{
				// data buffer allocation
				if( (Data = (DATASTRUCT *)malloc( sizeof(DATASTRUCT) )) == NULL )
				{
					RetVal = 0;
				}
			}
		break;
		
		default:
			RetVal = 0;
		break;
	}
	
	return RetVal;
}

/*!
** \fn short loader_DeInit( unsigned char TargetMemory )
** \brief DeInitialize the loader on the target memory
** \param TargetMemory The target memory
** \return 1: OK, 0: FAILED
**/
short loader_DeInit( unsigned char TargetMemory )
{
	short RetVal = 1;
	
	df_Deinit( EE_BANK_0 );
	
	if( Data != NULL )
	{
		free( Data );
		Data = NULL;
	}
	
	return RetVal;
}

#ifdef ENAB_BLANK_CHECK
/*!
** \fn short BlankCheck( unsigned char iSect )
** \brief Check the erased flash sector
** \param iSect The flash sector index
** \return 1: OK, 0: FAILED
**/ 
static short BlankCheck( unsigned char iSect )
{
	short RetVal = 0;
	unsigned long SectSize;
	unsigned long *pd;

	if( iSect < FLASH_NUMSECT )
	{
		// flash sector size in 32-bit word
		SectSize = (FlashSector_tab[iSect].EndAddress + 1 - FlashSector_tab[iSect].StartAddress)/sizeof(long);
		//SectSize = GetSectorSize( iSect )/sizeof(long);
		// blank check loop
		for( pd=(unsigned long *)FlashSector_tab[iSect].StartAddress; SectSize; SectSize-- )
		{
			Wdog();
			if( *pd != 0xFFFFFFFFL )
			{
				break;
			}
			pd++;
		}
		if( SectSize == 0 )
			RetVal = 1;
	}
	
	return RetVal;
}	
#endif	// ENAB_BLANK_CHECK

/*!
** \fn short WriteMem( unsigned char TargetMemory, unsigned long Addr,
** 						DATASTRUCT *Data, unsigned long size )
** \brief Write data to the target memory
** \param TargetMemory The target memory
** \param Addr The start address
** \param Data The data buffer
** \param size The number of bytes to write
** \return	0: OK
** 			-1: invalid target memory
** 			-2: verify error
** 			-3: blank-check error
**/
short WriteMem( unsigned char TargetMemory, unsigned long Addr,
				DATASTRUCT *Data, unsigned long size )
{
	short RetVal = 0;
	short i;
	//unsigned long *pd;
	// FLASH_EraseInitTypeDef EraseInitStruct;
#if !defined(ENAB_BLANK_CHECK) || !defined(ENAB_VERIFY)
	// uint32_t errorcode;
#endif
	// uint32_t SectorError = 0;
	//unsigned char *pb = NULL;
	
	switch( TargetMemory )
	{
		case MEM_ROMCODE:
		case MEM_ROMDATA:
			/* mark address limit *
			if( Addr+size >= ADDR_FIRMAAPPL )
			{
				size = (unsigned short)(ADDR_FIRMAAPPL - Addr);
			}
			**/
			// clear unused buffer (we write 32-bit data word into the destination flash)
			memset( &Data->DataByte[size], 0xFF, MAXDATASIZE-size );
			// do not write the boot sectors
			if( (Addr >= FlashSector_tab[APP_FIRST_SECTOR].StartAddress) &&
				//(Addr+size < ADDR_FIRMAAPPL )
				(Addr+size <= ADDR_FLASH_END )
				)
			{
			#ifdef ENAB_FLASH_WRITE
				/* Unlock the Flash to enable the flash control register access *************/ 
				// HAL_FLASH_Unlock();
				// while( FLASH_WaitForLastOperation(100) != HAL_OK );
				Flash_If_Init();
				// check if the destination flash sectors are to be erased
				for( i=APP_FIRST_SECTOR; i<FLASH_NUMSECT; i++ )
				{
					Wdog();
					if( (Addr >= FlashSector_tab[i].StartAddress) && (Addr <= FlashSector_tab[i].EndAddress) )
					{
						// erase the destination flash sectors
						if( !GetBitBMap( FlashSectErased, i ) )
						{
							Wdog();
							if( Flash_If_Erase_Sector( i ) != HAL_OK )
							{
							#ifndef ENAB_BLANK_CHECK
								RetVal = -3;
								break;
							#endif
							}
							/* Fill EraseInit structure*
							EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
							EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
							EraseInitStruct.Sector = i;
							EraseInitStruct.NbSectors = 1;
							if( HAL_FLASHEx_Erase(&EraseInitStruct, &SectorError) != HAL_OK )
							{
							#ifndef ENAB_BLANK_CHECK
								/* 
								  Error occurred while sector erase. 
								  User can add here some code to deal with this error. 
								  SectorError will contain the faulty sector and then to know the code error on this sector,
								  user can call function 'HAL_FLASH_GetError()'
								*
								errorcode = HAL_FLASH_GetError();
								RetVal = -3;
								break;
							#endif
							}*/
						#ifdef ENAB_BLANK_CHECK
							if( !BlankCheck( i ) )
							{
								RetVal = -3;
								break;
							}
						#endif										
							SetBitBMap( FlashSectErased, i );
						}
						if( (i < FLASH_NUMSECT-1) && (Addr+size > FlashSector_tab[i].EndAddress) )
						{
							if( !GetBitBMap( FlashSectErased, i+1 ) )
							{
								Wdog();
								if( Flash_If_Erase_Sector( i+1 ) != HAL_OK )
								{
								#ifndef ENAB_BLANK_CHECK
									RetVal = -3;
									break;
								#endif
								}
								/* Fill EraseInit structure*
								EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
								EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
								EraseInitStruct.Sector = i + 1;
								EraseInitStruct.NbSectors = 1;
								if( HAL_FLASHEx_Erase(&EraseInitStruct, &SectorError) != HAL_OK )
								{
								#ifndef ENAB_BLANK_CHECK
									/* 
									  Error occurred while sector erase. 
									  User can add here some code to deal with this error. 
									  SectorError will contain the faulty sector and then to know the code error on this sector,
									  user can call function 'HAL_FLASH_GetError()'
									*
									errorcode = HAL_FLASH_GetError();
									RetVal = -3;
									break;
								#endif
								}*/
							#ifdef ENAB_BLANK_CHECK
								if( !BlankCheck( i+1 ) )
								{
									RetVal = -3;
									break;
								}
							#endif										
								SetBitBMap( FlashSectErased, i+1 );
							}
						}
						// erase also the mark flash sector (the last one), if writing app code
						if( (Addr >= FlashSector_tab[APP_FIRST_SECTOR].StartAddress) &&
							!GetBitBMap( FlashSectErased, FLASH_NUMSECT-1 ) )
						{
							Wdog();
							if( Flash_If_Erase_Sector( FLASH_NUMSECT-1 ) != HAL_OK )
							{
							#ifndef ENAB_BLANK_CHECK
								RetVal = -3;
								break;
							#endif
							}
							/* Fill EraseInit structure*
							EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
							EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
							EraseInitStruct.Sector = FLASH_NUMSECT-1;
							EraseInitStruct.NbSectors = 1;
							if( HAL_FLASHEx_Erase(&EraseInitStruct, &SectorError) != HAL_OK )
							{
							#ifndef ENAB_BLANK_CHECK
								/* 
								  Error occurred while sector erase. 
								  User can add here some code to deal with this error. 
								  SectorError will contain the faulty sector and then to know the code error on this sector,
								  user can call function 'HAL_FLASH_GetError()'
								*
								errorcode = HAL_FLASH_GetError();
								RetVal = -3;
								break;
							#endif
							}*/
						#ifdef ENAB_BLANK_CHECK
							if( !BlankCheck( FLASH_NUMSECT-1 ) )
							{
								RetVal = -3;
								break;
							}
						#endif										
							SetBitBMap( FlashSectErased, FLASH_NUMSECT-1 );
						}
						break;
					}
				}
				/* Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
				 you have to make sure that these data are rewritten before they are accessed during code
				 execution. If this cannot be done safely, it is recommended to flush the caches by setting the
				 DCRST and ICRST bits in the FLASH_CR register. *
				__HAL_FLASH_DATA_CACHE_DISABLE();
				__HAL_FLASH_INSTRUCTION_CACHE_DISABLE();

				__HAL_FLASH_DATA_CACHE_RESET();
				__HAL_FLASH_INSTRUCTION_CACHE_RESET();

				__HAL_FLASH_INSTRUCTION_CACHE_ENABLE();
				__HAL_FLASH_DATA_CACHE_ENABLE();*/
			//#ifdef ENAB_BLANK_CHECK
				if( RetVal != -3 )
			//#endif										
				{
					// write 32-bit data words into the destination flash sectors
					if( size % sizeof(long) )
					{
						size = size/sizeof(long) + 1;
					}
					else
					{
						size = size/sizeof(long);
					}
					if( Flash_If_Program_WordBuffer( Addr, Data->DataDWord, size ) != HAL_OK )
					{
					#ifndef ENAB_VERIFY
						RetVal = -2;
						break;
					#endif
					}
					/*pd = (unsigned long *)Addr;
					for( i=0; i<size; i++ )
					{
						Wdog();
						if( HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, (uint32_t)pd, (uint64_t)Data->DataDWord[i]) != HAL_OK )
						{
						#ifndef ENAB_VERIFY
							/* Error occurred while writing data in Flash memory. 
							User can add here some code to deal with this error *
							errorcode = HAL_FLASH_GetError();
							RetVal = -2;
							break;
						#endif
						}
						pd++;
					}*/
				#ifdef ENAB_VERIFY							
					// verify data
					if( memcmp( Data->DataByte, (void *)Addr, (size*sizeof(long)) ) )
					{
						// verify error
						RetVal = -2;
					}
					/*pd = (unsigned long *)Addr;
					for( i=0; i<size; i++ )
					{
						Wdog();
						if( *pd != Data->DataDWord[i] )
						{
							// verify error
							RetVal = -2;
							break;
						}
						pd++;
					}*/
				#endif
				}
				/* Lock the Flash to disable the flash control register access (recommended
				 to protect the FLASH memory against possible unwanted operation) *********/
				// HAL_FLASH_Lock(); 
				Flash_If_Deinit();
			#endif	// ENAB_FLASH_WRITE
			}
		break;
		
		case MEM_DFDATA1:
		//case MEM_DFDATA2:
			// check if the destination dataflash subsectors are to be erased
			i = Addr / DF_SUBSECTOR_SIZE;
			// erase the destination dataflash sectors
			if( !GetBitBMap( DataFlashSubSectErased, i ) )
			{
				df_EraseSubSector( (TargetMemory - MEM_DFDATA1), i );
				SetBitBMap( DataFlashSubSectErased, i );
			}
			if( (i < NUM_SUBSECT_CHIP-1) && (((Addr+size)/DF_SUBSECTOR_SIZE) > i) )
			{
				if( !GetBitBMap( DataFlashSubSectErased, i+1 ) )
				{
					df_EraseSubSector( (TargetMemory - MEM_DFDATA1), i+1 );
					SetBitBMap( DataFlashSubSectErased, i+1 );
				}
			}
			// write data into the dataflash
			if( !df_Write( (TargetMemory - MEM_DFDATA1), Addr, Data->DataByte, size,
		#ifdef ENAB_VERIFY							
						WR_VER_YES ) )
		#else
						WR_VER_NONE ) )
		#endif
			{
				RetVal = -2;
			}
		break;

		/* case MEM_EEDATA1:
		case MEM_EEDATA2:
			// write data into the eeprom
			if( !eeWriteBuffer( (unsigned short)Addr, Data->DataByte, size) )
			{
			#ifndef ENAB_VERIFY							
				RetVal = -2;
			#endif
			}
		#ifdef ENAB_VERIFY							
			// verify data
			if( (pb = (unsigned char *)malloc( size )) != NULL )
			{
				eeReadBuffer( (unsigned short)Addr, pb, size );
				if( memcmp( Data->DataByte, pb, size ) )
				{
					// verify error
					RetVal = -2;
				}
				free( pb );
			}
			else
			{
				RetVal = -2;
			}
		#endif
		break; */
		
		default:
			RetVal = -1;
		break;
	}
	
	return RetVal;
}

/*!
** \fn short loader( unsigned char TargetMemory, const char *SRecord )
** \brief Load S-record data in the target memory
** \param TargetMemory The target memory
** \param SRecord The S-record string
** \return	1: end loading 
** 			0: record OK
** 			-1: invalid S-record
** 			-2: verify error
** 			-3: blank-check error
** 			-4: abort
** 			-5: erase/ID
**/
short loader( unsigned char TargetMemory, const char *SRecord )
{
	short RetVal = -1;
	unsigned long AbsAddr = 0x00000000;
	unsigned char RecordType;
	unsigned char nData;
	unsigned char Chk = 0x00;
	long i;

	i = strlen(SRecord);
	if( SRecord[0] == 'S' )
	{
		if( SRecord[1] == 'S' )
		{
			// Erase/ID command "SS"
			memset( FlashSectErased, 0, sizeof(FlashSectErased) );
			memset( DataFlashSubSectErased, 0, sizeof(DataFlashSubSectErased) );
			RetVal = -5;
		}
		else if( SRecord[1] == 'E' )
		{
			// Abort command "SE"
			RetVal = -4;
		}
		else if( CheckDigit( &SRecord[1] ) && (i <= SRECORD_MAXLEN) && (TargetMemory < NMEM) )
		{
			// record type
			RecordType = Ascii2Hex( SRecord[1] );
			// number of data bytes
			nData = 0x10*Ascii2Hex( SRecord[2] ) + 0x1*Ascii2Hex( SRecord[3] );
			Chk += nData;
			// process record type
			switch( RecordType )
			{
				case 0:
					// Header Record
				case 5:
					// Record to Manage the Number of Records
					RetVal = 0;
				break;
				
				case 1:
					// Data Record: 2-Byte Address
				case 2:
					// Data Record: 3-Byte Address
				case 3:
					// Data Record: 4-Byte Address
					switch( RecordType )
					{
						case 1:
							// 2-Byte Address + 1-byte checksum
							if( (i >= S1RECORD_MINLEN) && (nData > 3) )
							{
								RetVal = 0;
								// start address
								for( i=0; i<2; i++ )
								{
									Data->DataByte[i] = 0x10*Ascii2Hex( SRecord[4+2*i] ) + 0x1*Ascii2Hex( SRecord[5+2*i] );
									Chk += Data->DataByte[i];
									AbsAddr <<= 8;
									AbsAddr |= (unsigned long)Data->DataByte[i];
								}
								nData -= 2;
								// data
								for( i=0; i<nData; i++ )
								{
									Data->DataByte[i] = 0x10*Ascii2Hex( SRecord[8+2*i] ) + 0x1*Ascii2Hex( SRecord[9+2*i] );
									Chk += Data->DataByte[i];
								}
							}
						break;
						
						case 2:
							// 3-Byte Address + 1-byte checksum
							if( (i >= S2RECORD_MINLEN) && (nData > 4) )
							{
								RetVal = 0;
								// start address
								for( i=0; i<3; i++ )
								{
									Data->DataByte[i] = 0x10*Ascii2Hex( SRecord[4+2*i] ) + 0x1*Ascii2Hex( SRecord[5+2*i] );
									Chk += Data->DataByte[i];
									AbsAddr <<= 8;
									AbsAddr |= (unsigned long)Data->DataByte[i];
								}
								nData -= 3;
								// data
								for( i=0; i<nData; i++ )
								{
									Data->DataByte[i] = 0x10*Ascii2Hex( SRecord[10+2*i] ) + 0x1*Ascii2Hex( SRecord[11+2*i] );
									Chk += Data->DataByte[i];
								}
							}
						break;
						
						case 3:
							// 4-Byte Address + 1-byte checksum
							if( (i >= S3RECORD_MINLEN) && (nData > 5) )
							{
								RetVal = 0;
								// start address
								for( i=0; i<4; i++ )
								{
									Data->DataByte[i] = 0x10*Ascii2Hex( SRecord[4+2*i] ) + 0x1*Ascii2Hex( SRecord[5+2*i] );
									Chk += Data->DataByte[i];
									AbsAddr <<= 8;
									AbsAddr |= (unsigned long)Data->DataByte[i];
								}
								nData -= 4;
								// data
								for( i=0; i<nData; i++ )
								{
									Data->DataByte[i] = 0x10*Ascii2Hex( SRecord[12+2*i] ) + 0x1*Ascii2Hex( SRecord[13+2*i] );
									Chk += Data->DataByte[i];
								}
							}
						break;
					}
					// test checksum
					if( (RetVal == 0) && (Chk == 0xFF) )
					{
						// Data Record OK
						nData -= 1;	// the number of data bytes
						// write to the target memory
						RetVal = WriteMem( TargetMemory, AbsAddr, Data, nData );
					}
					else
					{
						// invalid S-record
						RetVal = -1;
					}
				break;
				
				case 7:
					// Terminator Record
				case 8:
					// Terminator Record
				case 9:
					// Terminator Record
					RetVal = 1;	// end loading
					if( (TargetMemory == MEM_ROMCODE) &&
						(
							GetBitBMap( FlashSectErased, FIRST_APPL_SECTOR_INDEX )
							|| GetBitBMap( FlashSectErased, SECOND_APPL_SECTOR_INDEX )
							|| GetBitBMap( FlashSectErased, THIRD_APPL_SECTOR_INDEX )
							|| GetBitBMap( FlashSectErased, FOURTH_APPL_SECTOR_INDEX )
						)
						)
					{
						// surely we wrote code data => mark flash on last sector
						memcpy( Data->DataByte, sFirma, FIRMA_LEN );
						RetVal = WriteMem( MEM_ROMCODE, ADDR_FIRMAAPPL, Data, FIRMA_LEN );
						if( RetVal == 0 )
						{
							RetVal = 1;	// end loading
						}
					}
				break;
			}
		}
	}
	
	return RetVal;
}
