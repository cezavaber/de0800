/*!
	\file DecodInput.c
	\brief Modulo per la decodifica degli ingressi
*/

#include "main.h"
#include "timebase.h"
#include "adc.h"
#include "DecodInput.h"
#include "rtc.h"

#ifdef LOGGING
	#define MONITOR_ADC		// abilita il monitor delle acquisizioni analogiche
#endif




#define T_VBATT_SAMPLE		(10)	//(12000)	// 20min, periodo di misura tensione batteria in scarica (0.1s)
#define T_VBATT_SAMPLE_ALIM	(10)	//(20)	// 2s, periodo di misura tensione batteria in carica (0.1s)



/* variabili inizializzate */



/* variabili non inizializzate */

unsigned short Vref;	//!< tensione VDDA/VREF+ (mV)

unsigned short VBatt;	//!< tensione batteria (mV)
unsigned short PercVBatt;	//!< percentuale di carica batteria (0.1%)


static unsigned short counter;
static unsigned short VBATTcounter;
static unsigned short adcVBatt;
static unsigned short adcVRefInt;
static unsigned short VrefInt;	// (mV) valore VREFINT calibrato

static unsigned char inpStat;

/*! \var DigInput
    \brief Bitmap ingressi digitali
*/
TBitByte DigInput;

static unsigned short filtPuls;	// filtro pulsante
static unsigned char filtAlim;	// filtro alimentazione esterna



/* costanti */


/*!
** \var VBatt_tab
** \brief Tabella dei parametri batteria
**/
BATT_STRUCT VBatt_tab =
{
	LITIO_VBATT_NOM,
	LITIO_VBATT_OFF,
	LITIO_VBATT_LOW,
	LITIO_VBATT_FULL,
	LITIO_VBATT_ANIN_NOM
};




/* codice */







/*! \fn void inpInit( void )
    \brief Inizializza gestore ingressi
*/
void inpInit( void )
{
	BATT_STRUCT *pt;
	
	adcInit();

	DIGIN = 0;
	filtPuls = 0;
	filtAlim = 0;
	counter = 0;
	VBATTcounter = 10;	//20;	// forza un aggiornamento della tensione batteria dopo un ciclo sufficiente di filtraggio analogico
	
	/*
		Inizializza le misure con i valori nominali,
		per compensare l'azione del filtraggio sulle prime letture
		che altrimenti fornirebbero un valore molto basso.
	*/
	pt = &VBatt_tab;
	VBatt = pt->VBattNom;
	Anain.VBatt = pt->VBattADNom;
	PercVBatt = 1000;
	Anain.VrefInt = VREFINT_ANIN_NOM;
	
	// valore VREFINT calibrato
	VrefInt = VREFINT_NOM( VREFINT_CAL );
#ifdef MONITOR_ADC
	myprintf( "VREFINT: %4u - %u.%03uV\r\n",
				VREFINT_CAL,
				(VrefInt/1000),
				(VrefInt%1000)
			);
#endif

	inpStat = 0;
}

/*! \fn void inpHandler( void )
    \brief Gestore ingressi
*/
void inpHandler( void )
{
	BATT_STRUCT *pt = &VBatt_tab;
	unsigned long d;
	
	switch( inpStat )
	{
		case 0:
			// check for start ADC scanning conversion every 5ms
			if( checktimer( T_ADC ) != INCORSO )
			{
				starttimer( T_ADC, 5 );
				
				// campionamento ingressi digitali
				filtPuls <<= 1;
				if( HAL_GPIO_ReadPin( PULS_GPIO_Port, PULS_Pin ) == GPIO_PIN_SET )
				{
					filtPuls |= BIT0;
				}
				if( filtPuls == 0xFFFF )
				{
					// pulsante premuto
					DIGIN_PULS = 1;
				}
				else if( filtPuls == 0x00 )
				{
					// pulsante rilasciato
					DIGIN_PULS = 0;
				}
				filtAlim <<= 1;
				if( HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET )
				{
					filtAlim |= BIT0;
				}
				if( filtAlim == 0xFF )
				{
					// alimentazione esterna presente
					DIGIN_ALIM = 1;
				}
				else if( filtAlim == 0x00 )
				{
					// alimentazione esterna assente
					DIGIN_ALIM = 0;
				}
				
				// gestione ingressi ogni 100ms
				if( ++counter >= 20 )
				{
					counter = 0;

					/*
						La VDDA attuale (VREF) viene determinata dalla lettura
						della VREFINT.
						
						VREFINT = (VREF/4095) * vADC
						
						VREF (mV)	= (4095 * VREFINT) / vADC
					*/
					adcVRefInt = Anain.VrefInt / ACCU_NUM_LETT;
					d = 40950;	// calcolo in 0.1mV
					d *= VrefInt;
					d /= adcVRefInt;
					d = (d + 5)/10;	// arrotondo a mV
					Vref = d;
					/*! TENSIONE BATTERIA */
					if( VBATTcounter )
					{
						VBATTcounter--;
					}
					if( VBATTcounter == 0 )
					{
						if( DIGIN_ALIM )
						{
							pt->VBattOff = LITIO_VBATT_OFF_ALIM;
							// riavvia timer aggiornamento batteria in carica
							VBATTcounter = T_VBATT_SAMPLE_ALIM;
						}
						else
						{
							pt->VBattOff = LITIO_VBATT_OFF;
							// riavvia timer aggiornamento batteria in scarica
							VBATTcounter = T_VBATT_SAMPLE;
						}
						/*
							La tensione viene ridotta da un partitore di un fattore 1/2 e poi
							acquisita dall'ADC 12-bit con VREF = 3V.

							Tensione (mV)	= 1000 * vADC * (VREF/4095) * (2) =
													= (vADC * 3000 * 2) / (4095) =
													= (vADC * 6000) / 4095

							La VREF e' variabile, percio' si rapporta il valore acquisito
							con il valore acquisito della VREFINT.

							Vbatt = 2*(VREF/4095) * vADC1
							VREFINT = (VREF/4095) * vADC2
							------------------------------
							Vbatt = (2 * vADC1 * VREFINT) / vADC2	indipendente dalla VREF
						*/
						adcVRefInt = Anain.VrefInt / ACCU_NUM_LETT;
						d = Anain.VBatt / ACCU_NUM_LETT;
						adcVBatt = d;
						d *= 20;	// calcolo in 0.1mV
						d *= VrefInt;
						d /= adcVRefInt;
						VBatt = (d + 5)/10;	// arrotondo a mV
						// percentuale di carica batteria
						if( VBatt > pt->VBattOff )
						{
							d = 10000*(VBatt - pt->VBattOff);	// calcolo in 0.01%
							/* if( DIGIN_ALIM
									//&& (BattUp == BATTERY_CHARGE)
								)
							{
								// dinamica tensione in carica
								d /= (pt->VBattFull - pt->VBattOff);
							}
							else
							{
								// dinamica tensione in scarica
								d /= (pt->VBattNom - pt->VBattOff);
							} */
							d /= (pt->VBattFull - pt->VBattOff);
							if( d > 10000 )
							{
								d = 10000;
							}
						}
						else
						{
							d = 0;
						}
						PercVBatt = (d + 5)/10;	// arrotondo a 0.1%
					}
				}

				ELAB_INP = 0;
				adcStart();
				inpStat = 1;
			}
		break;

		default:
		case 1:
			// a fine conversione disabilita DMA per ADC
			if( ELAB_INP )
			{
				ELAB_INP = 0;
				HAL_ADC_Stop_DMA( &hadc1 );
				inpStat = 0;
			}
		break;
		
		case 200:
			// sospensione
		break;
	}
}

/*! \fn void inpMonitor( void )
    \brief Monitor degli ingressi acquisiti
*/
void inpMonitor( void )
{
#ifdef MONITOR_ADC
	char s[30];

	myprintf( "\r\n\nVREF: %u.%03uV\r\n",
				(Vref/1000),
				(Vref%1000)
			);
	myprintf( "BATT: %4u - %u.%03uV [%u.%01u%%]\r\n",
				adcVBatt,
				(VBatt/1000),
				(VBatt%1000),
				(PercVBatt/10),
				(PercVBatt%10)
			);
	ScriviData( s );
	ScriviOra( &s[12] );
	myprintf("Data-Ora: %s %s [%u]\r\n", s, &s[12], SecCounter);
#endif	// MONITOR_ADC
}

/*!
 * \fn void inpRestart( void )
 * \brief Riavvia il gestore ingressi
 * \return None
 */
void inpRestart( void )
{
	adcInit();
	
	counter = 0;
	VBATTcounter = 10;	//20;	// forza un aggiornamento della tensione batteria dopo un ciclo sufficiente di filtraggio analogico
	ELAB_INP = 0;
	HAL_ADC_Stop_DMA( &hadc1 );
	HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
	
	// allinea filtri ingressi sull'ultimo stato acquisito
	/*if( DIGIN_PULS )
	{
		filtPuls = 0xFFFF;
	}
	else
	{
		filtPuls = 0x00;
	}*/
	filtPuls = 0x0001;	// forza un filtraggio completo del pulsante per la successiva decisione sul suo stato
	if( DIGIN_ALIM )
	{
		filtAlim = 0xFF;
	}
	else
	{
		filtAlim = 0x00;
	}
	
	inpStat = 0;
	starttimer( T_ADC, 25 );	// compensa l'eventuale transitorio sul power-on VBATT OPA
}

/*!
 * \fn void inpSuspend( void )
 * \brief Sospende il gestore ingressi
 * \return None
 */
void inpSuspend( void )
{
	inpStat = 200;
    HAL_NVIC_DisableIRQ(DMA2_Stream0_IRQn);
	ELAB_INP = 0;
	HAL_ADC_Stop_DMA( &hadc1 );
	// power off VBATT OPA
	HAL_GPIO_WritePin(EN_V_BATT_GPIO_Port, EN_V_BATT_Pin, GPIO_PIN_RESET);
}




