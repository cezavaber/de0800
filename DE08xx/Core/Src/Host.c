/*!
** \file Host.c
** \author Alessandro Vagniluca
** \date 23/12/2016
** \brief The serial Host bootloader handler
** 
** \version 1.0
**/

#include "main.h"
#include "Host.h"
#include "timebase.h"
#include "Loader.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"


#define  STX         0x02     /* STX */
#define  ACK         0x06     /* ACK */
#define  NACK        0x15     /* NACK */
#define  LF          0x0A     /* LF */
#define  CR          0x0D     /* CR */
#define  ENQ         0x05     /* ENQ */

#define	KEY_FLASH		0xAE
//#define	KEY_EEPROM1		0xAF
//#define	KEY_EEPROM2		0xB0
#define	KEY_DATAFLASH1	0xB1
//#define	KEY_DATAFLASH2	0xB2

#define TOUT_INIT_SHORT		(1*T1SEC)	// 1s
//#define TOUT_INIT_LONG		(10*T1SEC)	// 10s
#define TOUT_INIT_LONG		(15*T1SEC)	// 15s


/* serial RX buffer */
#define MAX_RXBUFFER		530	// >524 maximum ascii S-record length
static unsigned char *RxBuf = NULL;
static volatile unsigned long rxIns;
static volatile unsigned long rxEst;
static volatile unsigned long rxNum;	// 32-bit base type (atomic access)
/* serial TX buffer */
#define MAX_TXBUFFER		24
static unsigned char *TxBuf = NULL;

static char *Srec = NULL;


/*!
** \var bStatHost
** \brief The Host handler state
**/
enum
{
	HOST_STAT_IDLE,
	HOST_STAT_LINK_STX,
	HOST_STAT_LINK_CMD,
	HOST_STAT_RUN_PROG_FILE,
	HOST_STAT_RUN_END_OK,
	HOST_STAT_RUN_END_FAILED,
	NUM_HOST_STAT,
};
static unsigned char bStatHost;

/*!
** \var ErrCode
** \brief The Host handler error code
**/
enum
{
	HOST_ERR_NONE = 0,
	HOST_ERR_TIMEOUT = 1,
	HOST_ERR_S_REC = 2,
	HOST_ERR_OVER_BUFF = 4,
	HOST_ERR_ABORT = 5,
	HOST_ERR_VERIFY_EEP = 6,
	HOST_ERR_BLANK_FLASH = 7,
	HOST_ERR_VERIFY_FLASH = 0x10,
};
static short ErrCode;

static unsigned char TargetMemory;	//!< target memory code


/*!
** Host link commands.
** Format: 0x56|KEY_MEM|FIRMA|CR|LF
**/
#define CMDLEN				(12)
#define CMD_KEY_MEM_POS		(1)
static const char cmdLink_ROM[CMDLEN] = "\x56\xAE"FIRMA"\r\n";
//static const char cmdLink_EEP1[CMDLEN] = "\x56\xAF"FIRMA"\r\n";
//static const char cmdLink_EEP2[CMDLEN] = "\x56\xB0"FIRMA"\r\n";
static const char cmdLink_DF1[CMDLEN] = "\x56\xB1"FIRMA"\r\n";
//static const char cmdLink_DF2[CMDLEN] = "\x56\xB2"FIRMA"\r\n";



/*!
** \fn void RxData( unsigned char *Data, unsigned short len )
** \brief Store the received data
** \param Data Data buffer of the received data
** \param len  Number of the received bytes
** \return None
**/ 
void RxData( unsigned char *Data, unsigned short len )
{
	if( RxBuf != NULL )
	{
		while( len && (rxNum < MAX_RXBUFFER) )
		{
			RxBuf[rxIns] = *Data++;
			if( ++rxIns >= MAX_RXBUFFER )
			{
				rxIns = 0;
			}
			rxNum++;
			len--;
		}
	}
}

/*!
** \fn void SendBuffer( unsigned char *buff, unsigned short len )
** \brief Send a buffer
** \param [in] buff The buffer to send
** \param [in] len  The buffer size
** \return None
**/
static void SendBuffer( unsigned char *buff, unsigned short len )
{
	Wdog();
	while( CDC_Transmit_FS( NULL, 0 ) == USBD_BUSY );
	Wdog();
	CDC_Transmit_FS( buff, len );
}

/*!
** \fn void FlushRX( void )
** \brief Flush the RX queue
** \return None
**/
static void FlushRX( void )
{
	rxIns = 0;
	rxEst = 0;
	rxNum = 0;
}

/*!
** \fn int GetChar( unsigned char *pData )
** \brief Get the next received data byte
** \param [in] pData The received data byte pointer
** \return 1: a data byte was received, 0: otherwise
**/
static int GetChar( unsigned char *pData )
{
	int RetVal = 0;
	
	if( rxNum || (rxIns != rxEst) )
	{
		*pData = RxBuf[rxEst];
		if( ++rxEst >= MAX_RXBUFFER )
		{
			rxEst = 0;
		}
		if( rxNum )
		{
			rxNum--;
		}
		RetVal = 1;
	}
	
	return RetVal;
}

/*!
** \fn void Host_SendID( void )
** \brief Send ID and bootloader version
** \return None
**/
static void Host_SendID( void )
{
	unsigned char b[IDSK_LEN+6];
	int i;
	
	for( i=0; i<IDSK_LEN; i++ )
	{
		b[i] = ~sIdSk[i];
	}
	b[i++] = ~' ';
	b[i++] = ~(VERSIONE + '0');
	b[i++] = ~'.';
	b[i++] = ~(REVISIONE + '0');
	b[i++] = ~CR;
	b[i++] = ~LF;
	
	SendBuffer( b, i );
}

/*!
** \fn void Host_GestErr( short cod )
** \brief Error handler
** \param cod The error code
** \return None
**/
static void Host_GestErr( short cod )
{
	unsigned char b[2];
	unsigned char len = 0;
	
	// send NACK to Host
	b[len++] = NACK;
	if( cod >= 0 )
	{
		b[len++] = (unsigned char)('0' + cod);
	}
	SendBuffer( b, len );
}

/*!
** \fn void Host_End2Idle( unsigned char result )
** \brief End Host handler to idle state
** \param result 1: end OK, 0: end FAILED
** \return None
**/
static void Host_End2Idle( unsigned char result )
{
	if( result )
	{
		// OK
	#ifdef LOGGING
		myprintf( "PC SUCCESS\r\n" );
	#endif
		bStatHost = HOST_STAT_RUN_END_OK;
	}
	else
	{
		// FAILED
	#ifdef LOGGING
		myprintf( "PC FATAL ERROR\r\n" );
	#endif
		bStatHost = HOST_STAT_RUN_END_FAILED;
	}
	
	starttimer( THOST, 3*T1SEC );
}

/*!
** \fn short Host_Init( void )
** \brief Initialize Host handler
** \return 1: OK, 0: FAILED
**/
short Host_Init( void )
{
	short RetVal = 0;
	
	FlushRX();
	
	if( (RxBuf = (unsigned char *)malloc( MAX_RXBUFFER )) != NULL )
	{
		if( (TxBuf = (unsigned char *)malloc( MAX_TXBUFFER )) != NULL )
		{
			if( (Srec = (char *)malloc( MAX_RXBUFFER+1 )) != NULL )
			{
				memset( RxBuf, 0, MAX_RXBUFFER );
				memset( TxBuf, 0, MAX_TXBUFFER );
				memset( Srec, 0, MAX_RXBUFFER+1 );
				bStatHost = HOST_STAT_IDLE;
				HOST_RUN = 1;
				HOST_PROG = 0;
				RetVal = 1;
			}
			else
			{
				free( TxBuf );
				TxBuf = NULL;
				free( RxBuf );
				RxBuf = NULL;
			}
		}
		else
		{
			free( RxBuf );
			RxBuf = NULL;
		}
	}
	
	return RetVal;
}

/*!
** \fn short Host_Deinit( void )
** \brief End Host handler
** \return 1: OK, 0: FAILED
**/
short Host_Deinit( void )
{
	short RetVal = 0;
	
	if( TargetMemory < NMEM )
	{
		loader_DeInit( TargetMemory );
	}
	
	bStatHost = HOST_STAT_IDLE;
	HOST_RUN = 0;
	HOST_PROG = 0;
	
	if( Srec != NULL )
	{
		free( Srec );
		Srec = NULL;
	}
	if( TxBuf != NULL )
	{
		free( TxBuf );
		TxBuf = NULL;
	}
	if( RxBuf != NULL )
	{
		free( RxBuf );
		RxBuf = NULL;
	}
	
	return RetVal;
}

/*!
** \fn short Host_Handler( void )
** \brief Host handler
** \return One of the 'hostRetCode' values
**/
short Host_Handler( void )
{
	short RetVal = HOST_RET_IDLE;
	unsigned char rxChar;
	short i;

	if( HOST_RUN )
	{
		switch( bStatHost )
		{
			default:
				//bStatHost = HOST_STAT_IDLE;
			case HOST_STAT_IDLE:
				HOST_PROG = 0;
				TargetMemory = NMEM;
				FlushRX();
				bStatHost = HOST_STAT_LINK_STX;
			break;

			case HOST_STAT_LINK_STX:
				// waiting STX
				//if( GetChar( &rxChar ) )
				while( GetChar( &rxChar ) )
				{
					if( rxChar == STX )
					{
						// STX ok
						rxChar = ~STX;
						SendBuffer( &rxChar, 1 );
						ErrCode = 0;
						starttimer( THOST, TOUT_INIT_SHORT );
						bStatHost = HOST_STAT_LINK_CMD;
						break;
					}
				}
			break;

			case HOST_STAT_LINK_CMD:
				// waiting CMD
				//if( GetChar( &rxChar ) )
				while( GetChar( &rxChar ) )
				{
					starttimer( THOST, TOUT_INIT_SHORT );
					if( rxChar == STX )
					{
						// STX ok
						rxChar = ~STX;
						SendBuffer( &rxChar, 1 );
						ErrCode = 0;
					}
					else if( (rxChar == cmdLink_ROM[ErrCode])
							//|| (rxChar == cmdLink_EEP1[ErrCode])
							//|| (rxChar == cmdLink_EEP2[ErrCode])
							|| (rxChar == cmdLink_DF1[ErrCode])
							//|| (rxChar == cmdLink_DF2[ErrCode])
							)
					{
						if( ErrCode == CMD_KEY_MEM_POS )
						{
							// KEY_MEM
							switch( rxChar )
							{
								case KEY_FLASH:
									TargetMemory = MEM_ROMCODE;
								break;
								
								/* case KEY_EEPROM1:
									TargetMemory = MEM_EEDATA1;
								break; */
								
								/* case KEY_EEPROM2:
									TargetMemory = MEM_EEDATA2;
								break; */

								case KEY_DATAFLASH1:
									TargetMemory = MEM_DFDATA1;
								break;

								/* case KEY_DATAFLASH2:
									TargetMemory = MEM_DFDATA2;
								break; */
							}
						}
						if( ++ErrCode >= CMDLEN )
						{
							// set for a new S-record to be received
							ErrCode = 0;
							FlushRX();
							// CMD ok
							rxChar = ACK;
							SendBuffer( &rxChar, 1 );
							// start waiting S-record
							loader_Init( TargetMemory );
							HOST_PROG = 1;
							bStatHost = HOST_STAT_RUN_PROG_FILE;
							RetVal = HOST_RET_CONNECTED;
						#ifdef LOGGING
							myprintf( "PC connected for programming\r\n" );
						#endif
							break;
						}
					}
					else
					{
						// CMD error
					#ifdef LOGGING
						outbyte( 'C' );
					#endif
						Host_GestErr( ErrCode );
						bStatHost = HOST_STAT_IDLE;
						break;
					}
				}
				//else
				if( checktimer( THOST ) != INCORSO )
				{
					// rx timeout
				#ifdef LOGGING
					outbyte( 'T' );
				#endif
					Host_GestErr( ErrCode );
					bStatHost = HOST_STAT_IDLE;
				}
			break;

			case HOST_STAT_RUN_PROG_FILE:
				// waiting S-record
				RetVal = HOST_RET_CONNECTED;
				//if( GetChar( &rxChar ) )
				while( GetChar( &rxChar ) )
				{
					starttimer( THOST, TOUT_INIT_SHORT );
					RetVal = HOST_RET_RUNNING;
				#ifdef LOGGING
					//outbyte( rxChar );
				#endif
					Srec[ErrCode] = rxChar;
					if( rxChar == LF )
					{
						// 'ErrCode' is the received S-record length in the 'RxBuf' buffer
						/* // discard CR e LF
						if( RxBuf[ErrCode-1] == CR )
						{
							ErrCode--;
						}
						RxBuf[ErrCode] = '\0';	// S-record string end
						// set for a new S-record to be received
						ErrCode = 0;
						FlushRX();
						// S-record loader
						i = loader( TargetMemory, (const char *)RxBuf ); */
						// discard CR e LF
						if( Srec[ErrCode-1] == CR )
						{
							ErrCode--;
						}
						Srec[ErrCode] = '\0';	// S-record string end
						// set for a new S-record to be received
						ErrCode = 0;
						FlushRX();
						// S-record loader
						i = loader( TargetMemory, (const char *)Srec );
						switch( i )
						{
							case 0:
								// S-record OK
								rxChar = ACK;
								SendBuffer( &rxChar, 1 );
							#ifdef LOGGING
								outbyte( '.' );
							#endif
							break;
							
							case 1:
								// end loading
								rxChar = ACK;
								SendBuffer( &rxChar, 1 );
							#ifdef LOGGING
								myprintf( "\r\nEnd Loading\r\n" );
							#endif
								loader_DeInit( TargetMemory );
								Host_End2Idle( 1 );
								RetVal = HOST_RET_END_OK;
							break;

							case -5:
								// Erase/ID command "SS"
							#ifdef LOGGING
								outbyte( 'S' );
							#endif
								// send ID
								Host_SendID();
								starttimer( THOST, TOUT_INIT_LONG );	// longer timeout
							break;
							
							case -4:
								// Abort command "SE"
							#ifdef LOGGING
								outbyte( 'A' );
							#endif
								loader_DeInit( TargetMemory );
								Host_GestErr( HOST_ERR_ABORT );
								Host_End2Idle( 0 );
								RetVal = HOST_RET_END_FAILED;
							break;
							
							case -1:
								// invalid S-record
							#ifdef LOGGING
								outbyte( 'R' );
							#endif
								Host_GestErr( HOST_ERR_S_REC );
							break;
							
							case -3:
								// blank-check error
							#ifdef LOGGING
								outbyte( 'B' );
							#endif
								loader_DeInit( TargetMemory );
								Host_GestErr( HOST_ERR_BLANK_FLASH );
								Host_End2Idle( 0 );
								RetVal = HOST_RET_END_FAILED;
							break;
							
							case -2:
								// verify error
							#ifdef LOGGING
								outbyte( 'V' );
							#endif
								loader_DeInit( TargetMemory );
								/* if( (TargetMemory == MEM_EEDATA1)
									|| (TargetMemory == MEM_EEDATA2)
									)
								{
									Host_GestErr( HOST_ERR_VERIFY_EEP );
								}
								else */
								{
									Host_GestErr( HOST_ERR_VERIFY_FLASH );
								}
								Host_End2Idle( 0 );
								RetVal = HOST_RET_END_FAILED;
							break;
						}
						break;
					}
					else if( ++ErrCode >= MAX_RXBUFFER )
					{
						// buffer error
					#ifdef LOGGING
						outbyte( 'O' );
					#endif
						loader_DeInit( TargetMemory );
						Host_GestErr( HOST_ERR_OVER_BUFF );
						Host_End2Idle( 0 );
						RetVal = HOST_RET_END_FAILED;
						break;
					}
				}
				//else
				if( checktimer( THOST ) != INCORSO )
				{
					// rx timeout
				#ifdef LOGGING
					outbyte( 'T' );
				#endif
					loader_DeInit( TargetMemory );
					Host_GestErr( HOST_ERR_TIMEOUT );
					Host_End2Idle( 0 );
					RetVal = HOST_RET_END_FAILED;
				}
			break;
			
			case HOST_STAT_RUN_END_OK:
				RetVal = HOST_RET_END_OK;
				//if( checktimer( THOST ) != INCORSO )	return to wait for another possible programming file
				{
					bStatHost = HOST_STAT_IDLE;
				}
			break;
			
			case HOST_STAT_RUN_END_FAILED:
				RetVal = HOST_RET_END_FAILED;
				//if( checktimer( THOST ) != INCORSO )	return to wait for another possible programming file
				{
					bStatHost = HOST_STAT_IDLE;
				}
			break;
		}
	}
	
	// if returns in idle state, restart timeout for Host handler enabled
	if( bStatHost == HOST_STAT_IDLE )
	{
		starttimer( THOST, HOST_TIMEOUT );
	}
	
	return RetVal;
}


/*!
** \fn short Host_IsEnabled( void )
** \brief Check if the Host handler is enabled
** \return 1: ENABLED, 0: DISABLED
**/
short Host_IsEnabled( void )
{
	short RetVal = 0;
	
	if(
		(HAL_GPIO_ReadPin( PRES_RETE_GPIO_Port, PRES_RETE_Pin ) == GPIO_PIN_SET)
		&& (APP_NOT_PRESENT
			|| (checktimer( THOST ) == INCORSO)
			|| (bStatHost >= HOST_STAT_LINK_CMD))
		)
	{
		RetVal = 1;
	}
	
	return RetVal;
}



