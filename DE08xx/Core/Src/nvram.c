/***************************************************************************
* File      : NVRAM.C
* Scopo     : Modulo di gestione RAM non volatile
* Autore    : A. Vagniluca
***************************************************************************/

#include "main.h"
#include "nvram.h"


/* defines */



/* variabili */
/* variabili non volatili (escluse dal clear ram) */

NVRAMDATA NVRam;


/* prototipi */
static void ResetStatSec( void );
static BYTE CheckPar( void );





/***************************************************************************
* FUNZIONE  : short nvramInit( void )
* SCOPO     : Inizializza gestione RAM non volatile
* ARGOMENTI :
* RETURN    : 1: OK, 0: default
***************************************************************************/
short nvramInit( void )
{
	short RetVal = 1;
	unsigned short i;
	unsigned char AllNull = 1;
	
	// legge la NVRAM
	for( i=0; i<NVRAM_SIZE; i++ )
	{
		NVRam.Array[i] = HAL_RTCEx_BKUPRead( &hrtc, RTC_BKP_DR0+i );
		if( NVRam.Array[i] )
		{
			// almeno una word � non nulla
			AllNull = 0;
		}
	}
	
	// controlla la NVRAM (tutta azzerata oppure checksum)
	if( AllNull ||
		(NVChecksum != CalcChecksum( NVRam.NVData.uStatSec.b, STAT_SEC_UNION_SIZE ))
		)
	{
		// azzero tutta la NVRAM letta
		memset( NVRam.Array, 0, sizeof(NVRam.Array) );
		
		// configurazione al default
		CfgDef();

		RetVal = 0;
	}

	CheckPar();
		
	/* recupero la lingua corrente *
	iLanguage = StatSec.Language;
	if( iLanguage == LANG_DEF )
		iLanguage = LANG_ITA;	// lingua di default
	*/
	
	return RetVal;
}

/*!
** \fn short nvramUpdate( void )
** \brief Update the NVRAM
** \return 1: OK, 0: FAILED
**/
short nvramUpdate( void )
{
	short RetVal = 1;
	unsigned short i;
	
	// aggiorno il checksum
	NVChecksum = CalcChecksum( NVRam.NVData.uStatSec.b, STAT_SEC_UNION_SIZE );
	// scrive la NVRAM
	for( i=0; i<NVRAM_SIZE; i++ )
	{
		 HAL_RTCEx_BKUPWrite( &hrtc, RTC_BKP_DR0+i, NVRam.Array[i] );
	}
	
	return RetVal;
}

/***************************************************************************
* FUNZIONE  : void CfgDef( void )
* SCOPO     : Copia in ram i parametri di default
* ARGOMENTI :
* RETURN    :
***************************************************************************/
void CfgDef( void )
{
	CronoDef();
	RFid = RFID_DEF;
	ResetStatSec();
}

/****************************************************************************
* FUNZIONE : void ResetStatSec( void )
* SCOPO    : Inizializza ultimi stato scheda e numero di secondi
****************************************************************************/
static void ResetStatSec( void )
{
	PAN_BYTE_SETS = 0;
	F_TONI_ON_PAN	= 0;
	STATO_CRONO = 0;
	SHOWTEMP = SHOW_TEMP_AUTO;

	StatSec.LastStatoFunz = 0;		// stato di reset
	StatSec.LastNumSec = 0;
	StatSec.Language = LANG_DEF;
	StatSec.BackLightTime = 1;		// 10s di backlight al 100%
	StatSec.AccConsumo = 0;
	StatSec.NumNoAcc = 0;
	StatSec.UserColorID = UCOL_BLUE;
}


/***************************************************************************
* FUNZIONE  : void CronoDef( void )
* SCOPO     : Dati Crono al default
* ARGOMENTI :
* RETURN    :
***************************************************************************/
void CronoDef( void )
{
	unsigned char i;
	CRONO_PROG_WEEK *prw;

	Crono.CronoMode.BYTE = 0;	// CRONO_OFF, iProfile=0 (nessuno)
	for( i=0; i<NPROGWEEK; i++ )
	{
		prw = &Crono.Crono_ProgWeek[i];
		prw->OraStart = PROG_OFF;
		prw->OraStop = PROG_OFF;
		prw->TempAria = DEFAULTSETTEMP;
		prw->TempH2O = H2O_DEFAULT;
		prw->Fire = POTDEFAULT;
		prw->Enab.BYTE = 0;
	}
}

/***************************************************************************
* FUNZIONE  : BYTE CheckPar( void )
* SCOPO     : Controlla la coerenza dei parametri. Se necessario assegna il default.
* ARGOMENTI :
* RETURN    : 1: parametri OK, 0: corretti uno o piu' parametri
***************************************************************************/
static BYTE CheckPar( void )
{
	BYTE RetVal = 1;
	unsigned char i,j;
	CRONO_PROG_WEEK *prw;

	if( Crono.CronoMode.BIT.iProfile >= NUMCRONOPROFILE )
	{
		Crono.CronoMode.BIT.iProfile = 0;	// nessuno
		RetVal = 0;
	}

	for( i=0,j=0; i<NPROGWEEK; i++ )
	{
		prw = &Crono.Crono_ProgWeek[i];
		if( (prw->OraStart > 95) && (prw->OraStart != PROG_OFF) )
		{
			prw->OraStart = PROG_OFF;
			RetVal = 0;
		}
		if( ((prw->OraStop < 1) || (prw->OraStop > 96)) && (prw->OraStop != PROG_OFF) )
		{
			prw->OraStop = PROG_OFF;
			RetVal = 0;
		}
		if( prw->OraStop < prw->OraStart )
		{
			prw->OraStart = PROG_OFF;
			prw->OraStop = PROG_OFF;
			RetVal = 0;
		}
		if( prw->Enab.BIT.WeekEnab &&
			((prw->OraStart == PROG_OFF) ||
			(prw->OraStop == PROG_OFF) ||
			!(prw->Enab.BYTE & 0x7F)) )
		{
			prw->Enab.BIT.WeekEnab = 0;
			RetVal = 0;
		}
		if( (prw->TempAria < MINSETTEMP) || (prw->TempAria > MAXSETTEMP) )
		{
			prw->TempAria = DEFAULTSETTEMP;
			RetVal = 0;
		}
		if( (prw->TempH2O < H2O_TMIN) || (prw->TempH2O > H2O_TMAX) )
		{
			prw->TempH2O = H2O_DEFAULT;
			RetVal = 0;
		}
		if( (prw->Fire < POTMINLEV) || (prw->Fire > POTMAXLEV) )
		{
			prw->Fire = POTDEFAULT;
			RetVal = 0;
		}
		if( prw->Enab.BIT.WeekEnab )
			j++;
	}

	// se nessun programma e` abilitato, il crono e` disabilitato
	if( j == 0 )
		Crono.CronoMode.BIT.Enab = CRONO_OFF;

	// allineamento con eventuale profilo crono preimpostato presente
	//Crono.CronoMode.BIT.iProfile = IsCronoProfile();

	// ID radiocomando diverso da RFID_JOLLY
	if( RFid == RFID_JOLLY )
	{
		RFid = RFID_DEF;
		RetVal = 0;
	}

	return RetVal;
}

/***************************************************************************
* FUNZIONE  : void RecConfig( void )
* SCOPO     : Registra la configurazione
* ARGOMENTI :
* RETURN    :
***************************************************************************/
void RecConfig( void )
{
	// NVRAM is updated every 1s
}

/****************************************************************************
* FUNZIONE  : void ScriviCrono(BYTE ByValue)
* SCOPO     : Scrittura abilitazione crono.
* ARGOMENTI : ByValue - nuovo valore abilitazione crono
* RETURN    :
*****************************************************************************/
void ScriviCrono(BYTE ByValue)
{
	Crono.CronoMode.BIT.Enab = ByValue ? CRONO_ON : CRONO_OFF;
}

/****************************************************************************
* FUNZIONE  : void ScriviCronoProfile(BYTE ByValue)
* SCOPO     : Scrittura indice profilo crono preimpostato.
* ARGOMENTI : ByValue - nuovo valore indice profilo crono preimpostato
* RETURN    :
*****************************************************************************/
void ScriviCronoProfile(BYTE ByValue)
{
	if( (Crono.CronoMode.BIT.iProfile != ByValue) && (ByValue < NUMCRONOPROFILE) )
	{
		Crono.CronoMode.BIT.iProfile = ByValue;
	}
}

/****************************************************************************
* FUNZIONE  : void ScriviCronoxxx( BYTE index, BYTE val )
* SCOPO     : Scrittura dei singoli dati CRONO.
* ARGOMENTI : index - indice programma crono
*				  val - nuovo valore parametro programma crono
* RETURN    :
*****************************************************************************/

void ScriviCronoProgWeekStart( BYTE index, BYTE val )
{
	BYTE *pb = &Crono.Crono_ProgWeek[index].OraStart;

	if( val != *pb )
	{
		*pb = val;
		if( *pb >= 96 )
			*pb = PROG_OFF;
	}
}

void ScriviCronoProgWeekStop( BYTE index, BYTE val )
{
	BYTE *pb = &Crono.Crono_ProgWeek[index].OraStop;

	if( val != *pb )
	{
		*pb = val;
		if( (*pb < 1) || (*pb > 96) )
			*pb = PROG_OFF;
	}
}

void ScriviCronoProgWeekAbil( BYTE index, BYTE val )
{
	BYTE *pb = &Crono.Crono_ProgWeek[index].Enab.BYTE;

	if( val != *pb )
	{
		*pb = val;
	}
}

void ScriviCronoProgWeekTempAria( BYTE index, BYTE val )
{
	BYTE *pb = &Crono.Crono_ProgWeek[index].TempAria;

	if( val != *pb )
	{
		*pb = val;
	}
}

void ScriviCronoProgWeekTempH2O( BYTE index, BYTE val )
{
	BYTE *pb = &Crono.Crono_ProgWeek[index].TempH2O;

	if( val != *pb )
	{
		*pb = val;
	}
}

void ScriviCronoProgWeekFire( BYTE index, BYTE val )
{
	BYTE *pb = &Crono.Crono_ProgWeek[index].Fire;

	if( val != *pb )
	{
		*pb = val;
	}
}

/****************************************************************************
* FUNZIONE  : void ScriviRFid( BYTE ByValue )
* SCOPO     : Scrittura ID radiocomando.
* ARGOMENTI : ByValue - nuovo valore ID radiocomando
* RETURN    :
*****************************************************************************/
void ScriviRFid( BYTE ByValue )
{
	if( (RFid != ByValue) && (ByValue != RFID_JOLLY) )
	{
		RFid = ByValue;
	}
}





