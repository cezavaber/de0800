/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include <stdlib.h>
#include <string.h>
#include "typedefine.h"
#include "Utils.h"
#include "printf-stdarg.h"
#include "dwt_stm32_delay.h"
#include "DatiStufa.h"


/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CHARGE_STAT_Pin GPIO_PIN_13
#define CHARGE_STAT_GPIO_Port GPIOC
#define INT1_ACC_Pin GPIO_PIN_1
#define INT1_ACC_GPIO_Port GPIOC
#define INT1_ACC_EXTI_IRQn EXTI1_IRQn
#define USART2_CTS_Pin GPIO_PIN_1
#define USART2_CTS_GPIO_Port GPIOA
#define SPI1_NSS_Pin GPIO_PIN_4
#define SPI1_NSS_GPIO_Port GPIOA
#define PD_FT801_Pin GPIO_PIN_4
#define PD_FT801_GPIO_Port GPIOC
#define INT2_ACC_Pin GPIO_PIN_5
#define INT2_ACC_GPIO_Port GPIOC
#define INT2_ACC_EXTI_IRQn EXTI9_5_IRQn
#define INT_FT801_Pin GPIO_PIN_1
#define INT_FT801_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define SPI2_NSS_Pin GPIO_PIN_12
#define SPI2_NSS_GPIO_Port GPIOB
#define BUZZER_Pin GPIO_PIN_13
#define BUZZER_GPIO_Port GPIOB
#define CONF_RF_Pin GPIO_PIN_14
#define CONF_RF_GPIO_Port GPIOB
#define PD_RF_Pin GPIO_PIN_15
#define PD_RF_GPIO_Port GPIOB
#define EN_FLASH_Pin GPIO_PIN_8
#define EN_FLASH_GPIO_Port GPIOC
#define RESET_RF_Pin GPIO_PIN_9
#define RESET_RF_GPIO_Port GPIOC
#define USART2_RTS_Pin GPIO_PIN_8
#define USART2_RTS_GPIO_Port GPIOA
#define EN_V_BCKL_Pin GPIO_PIN_10
#define EN_V_BCKL_GPIO_Port GPIOA
#define PRES_RETE_Pin GPIO_PIN_3
#define PRES_RETE_GPIO_Port GPIOB
#define PULS_Pin GPIO_PIN_4
#define PULS_GPIO_Port GPIOB
#define EN_V_BATT_Pin GPIO_PIN_5
#define EN_V_BATT_GPIO_Port GPIOB
#define EN_PWR_Pin GPIO_PIN_9
#define EN_PWR_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

// versione firmware
#define codSW		"DE08"
#define verSW		0
#define revSW		1
#define buildSW		2
#define sFW_VERSIONE	"DE080001_02"


/*!
** \def BOOT_TIMEOUT
** \brief The bootloader timeout for launch the application
**/
#define BOOT_TIMEOUT	(3*T1SEC)	// 3s
/*!
** \def HOST_TIMEOUT
** \brief The Host process timeout
**/
#define HOST_TIMEOUT	(30*T1SEC)	// 30s

extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;

extern I2C_HandleTypeDef hi2c1;

extern IWDG_HandleTypeDef hiwdg;

extern RTC_HandleTypeDef hrtc;

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;

extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart6;

enum
{
	CLK_SLOW,	//!< SPI1 clock: 1 MHz
	CLK_FAST,	//!< SPI1 clock: 21 MHz
	NUM_CLK_MODE
};

/*! \var SystemFlags
    \brief Bitmap flag di sistema
*/
extern TBitDWord SystemFlags;
#define FLAGS				SystemFlags.DWORD
//#define SD_RUN				SystemFlags.BIT.D0	//!< =1 SD card is being accessed
//#define USB_MSD_RUN			SystemFlags.BIT.D1	//!< =1 USB MSD is being accessed
#define HOST_RUN			SystemFlags.BIT.D2	//!< =1 serial Host is running
#define APP_NOT_PRESENT		SystemFlags.BIT.D3	//!< =1 a valid application is not present
//#define SD_PROG				SystemFlags.BIT.D4	//!< =1 SD card loader is programming
//#define USB_MSD_PROG		SystemFlags.BIT.D5	//!< =1 USB MSD loader is programming
#define HOST_PROG			SystemFlags.BIT.D6	//!< =1 serial Host is programming
#define ELAB_INP				SystemFlags.BIT.D7	//!< =1 attiva elaborazione ingressi
#define PULS_INP				SystemFlags.BIT.D8	//!< =1 pushbutton is pressed
#define RTC_TIME				SystemFlags.BIT.D9	//!< =1 avvenuto tick RTC (ogni 1s)
#define ALIM_INP				SystemFlags.BIT.DA	//!< =1 external supply is present
#define WAS_STANDBY				SystemFlags.BIT.D18	//!< il flag SBF riletto all'avvio nel registro PWR_CSR
#define WAS_WAKEUP				SystemFlags.BIT.D19	//!< il flag WUF riletto all'avvio nel registro PWR_CSR
#define WAS_IWDG				SystemFlags.BIT.D1A	//!< il flag IWDGRSTF riletto all'avvio nel registro RCC_CSR


/*!
** \def debug_out()
** \brief putchar() function (polling)
** \note The USART must be already initialized for logging
**/
#define debug_out(x)				\
	do {	\
		uint16_t tmp = (uint16_t)(x) & 0x01FFU;	\
		while( __HAL_UART_GET_FLAG(&huart6, UART_FLAG_TXE) == FALSE );	\
		huart6.Instance->DR = tmp;	\
	} while(0)

/*!
**  \def Wdog
**  \brief The refresh watchdog function
**/
#ifdef WDOG
	#define Wdog()		HAL_IWDG_Refresh(&hiwdg)
#else
	#define Wdog()		__NOP()
#endif


// valid application mark
#define FIRMA_LEN		8
//					 01234567
#define FIRMA		"VBR-BOOT"
extern const char sFirma[FIRMA_LEN+1];
// ID board
#define IDSK_LEN		4
extern const char sIdSk[IDSK_LEN+1];
// bootloader firmware version
#define VERSIONE  		4
#define REVISIONE 		0

/*!
** \fn void MX_SPI1_Init(unsigned char mode)
** \brief SPI1 init function
** \param mode The SPI clock configuration mode (CLK_SLOW or CLK_FAST)
** \return None
**/
void MX_SPI1_Init(unsigned char mode);


#ifdef LOGGING
/**
  * @fn		int outbyte( int c )
  * @brief  Retargets the C library printf function (fputc) to the USART.
  * @param  c The character to print
  * @retval The printed character
  */
int outbyte( int c );

/*!
**  \fn short LOCKprintf( void )
**  \brief Lock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short LOCKprintf( void );

/*!
**  \fn short UNLOCKprintf( void )
**  \brief Unlock the printf() resource
**  \return 1: OK, 0: FAILED
**/
short UNLOCKprintf( void );
#endif




/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
