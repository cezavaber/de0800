/*!
** \file Host.h
** \author Alessandro Vagniluca
** \date 23/12/2016
** \brief HOST.C header file
** 
** \version 1.0
**/

#ifndef __HOST_H
#define __HOST_H

#ifdef __cplusplus
extern "C" {
#endif


/*!
** \fn short Host_Init( void )
** \brief Initialize Host handler
** \return 1: OK, 0: FAILED
**/
short Host_Init( void );

/*!
** \fn short Host_Deinit( void )
** \brief End Host handler
** \return 1: OK, 0: FAILED
**/
short Host_Deinit( void );

/*!
** \enum hostRetCode
** \brief The Host handler return codes
**/
enum hostRetCode
{
	HOST_RET_IDLE,
	HOST_RET_CONNECTED,
	HOST_RET_RUNNING,
	HOST_RET_END_OK,
	HOST_RET_END_FAILED,
};

/*!
** \fn short Host_Handler( void )
** \brief Host handler
** \return One of the 'hostRetCode' values
**/
short Host_Handler( void );

/*!
** \fn short Host_IsEnabled( void )
** \brief Check if the Host handler is enabled
** \return 1: ENABLED, 0: DISABLED
**/
short Host_IsEnabled( void );


#ifdef __cplusplus
}
#endif

#endif	// __HOST_H
