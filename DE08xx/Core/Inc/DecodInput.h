/*!
	\file DecodInput.h
	\brief Include file del modulo DecodInput.c
*/

#ifndef _DECODINPUT_H
#define _DECODINPUT_H




/*! \var DigInput
    \brief Bitmap ingressi digitali
*/
extern TBitByte DigInput;
#define DIGIN						DigInput.BYTE
#define DIGIN_PULS					DigInput.BIT.D0	// =1 pulsante premuto
#define DIGIN_ALIM					DigInput.BIT.D1	// =1 alimentazione esterna presente



/*!
** \var VBatt_tab
** \brief Tabella dei parametri batteria
**/
typedef struct
{
	unsigned short VBattNom;	//!< livello tensione batteria nominale (mV)
	unsigned short VBattOff;	//!< livello tensione batteria scarica (mV)
	unsigned short VBattLow;	//!< basso livello tensione batteria (mV)
	unsigned short VBattFull;	//!< max livello tensione batteria carica (mV)
	unsigned short VBattADNom;	//!< valore A/D accumulato per tensione batteria nominale
} BATT_STRUCT;
extern BATT_STRUCT VBatt_tab;

extern unsigned short VBatt;	//!< tensione batteria (mV)
extern unsigned short PercVBatt;	//!< percentuale di carica batteria (0.1%)


/*
	La tensione viene ridotta da un partitore di un fattore 1/2 e poi
	acquisita dall'ADC 12-bit con VREF = 3V.

	Tensione (mV)	= 1000 * vADC * (VREF/4095) * (2) =
							= (vADC * 3000 * 2) / (4095) =
							= (vADC * 6000) / 4095
*/
#define LITIO_VBATT_NOM			(3700)	// tensione batteria nominale (mV)
#define LITIO_VBATT_ANIN_NOM	(2525)	//(20202)	// valore A/D accumulato per tensione batteria nominale
// batteria al litio 3.7 V ricaricabile
#define LITIO_VBATT_OFF		(3500)	// batteria scarica in scarica: 3.5 V
#define LITIO_VBATT_OFF_ALIM	(3550)	// batteria scarica in carica: 3.55 V
#define LITIO_VBATT_LOW		(3500)	//(3410)	// soglia di forzatura low-power: 3.5 V
#define LITIO_VBATT_FULL	(4100)	// batteria completamente carica: 4.1 V


/* Value of analog reference voltage (Vref+), connected to analog voltage   */
/* supply Vdda (unit: mV).                                                  */
#define VDDA_APPLI                       ((uint32_t)3000)	// 3.0V
/* Internal voltage reference VrefInt */
#define VREFINT_CAL_ADDR                   ((uint16_t*) (0x1FFF7A2AU)) /* Internal voltage reference, address of parameter VREFINT_CAL: VrefInt ADC raw data acquired at temperature 30 DegC (tolerance: +-5 DegC), Vref+ = 3.3 V (tolerance: +-10 mV). */
/*
	La tensione VREFINT (1.21 V) viene acquisita dall'ADC 12-bit con VREF = VDDA_APPLI.

	VREFINT (mV)	= 1000 * vADC * (VREF/4095) =
							= (vADC * VDDA_APPLI) / 4095
	
	Il valore VREFINT_CAL acquisito in produzione dall'ADC con VREF = 3,3V e'
	disponibile alla locazione 0x1FFF7A2A, per cui:
	
	VREFINT (mV)	= 1000 * VREFINT_CAL * (VREF/4095) =
							= (VREFINT_CAL * 3300) / 4095
	
*/
#define VREFINT_CAL			(*VREFINT_CAL_ADDR)
//#define VREFINT_NOM			(1210)	// tensione VREFINT nominale (mV)
#define VREFINT_NOM(x)			(((x) * 3300UL) / 4095UL)	// tensione VREFINT nominale (mV)
#define VREFINT_ANIN_NOM	(1652)	//(13213)	// valore A/D accumulato per tensione VREFINT





/*! \fn void inpInit( void )
    \brief Inizializza gestore ingressi
*/
void inpInit( void );

/*! \fn void inpHandler( void )
    \brief Gestore ingressi
*/
void inpHandler( void );

/*! \fn void inpMonitor( void )
    \brief Monitor degli ingressi acquisiti
*/
void inpMonitor( void );

/*!
 * \fn void inpRestart( void )
 * \brief Riavvia il gestore ingressi
 * \return None
 */
void inpRestart( void );

/*!
 * \fn void inpSuspend( void )
 * \brief Sospende il gestore ingressi
 * \return None
 */
void inpSuspend( void );



#endif	// _DECODINPUT_H
