/***************************************************************************
* File      : NVRAM.H
* Scopo     : File header per il modulo NVRAM.C
***************************************************************************/

#ifndef _NVRAM_H_
#define _NVRAM_H_

#include "crono.h"

// struttura RAM non volatile

typedef union
{
	struct
	{
		unsigned reserved1						:1;		//
		unsigned unEnToni						:1;		// Abilitazioni toni.
		unsigned unStatoCrono				:1;		// Identifico la fascia di funzionamento 1=stufa accesa.
		unsigned EnBoot						:1;		// =1 abilita bootloader
		unsigned ShowTemp						:2;		// Visualizzazione temperature: 0=AUTO, 1=Celsius, 2=Fahrenheit
		unsigned VisSanitari					:1;		// =1 visualizza set sanitari in AVVIO 
		unsigned VisTipoStufa				:1;		// =1 abilita modifica tipo stufa da AVVIO
	} BIT_SETS;
	unsigned char BYTE_SETS;
} PANSET;

typedef struct __packed
{
	CRONOSTRUCT CronoPar;	// struttura Crono
	unsigned char RFidPar;	// ID radiocomando
	PANSET PanSet;							// flags
	unsigned char LastStatoFunz;		// ultimo stato funzionale della scheda prima del reset
	unsigned long LastNumSec;			// ultimo numero di secondi dal 01/01/MIN_YEAR 00.00.00 prima del reset
	unsigned char Language;				// indice lingua per visualizzazioni
	unsigned char BackLightTime;		// tempo di backlight LCD al 100% (step di 10s; 0: sempre acceso)
	unsigned long AccConsumo;			// pellet consumato (0.1g)
	unsigned short NumNoAcc;			// numero mancate accensioni
	unsigned char UserColorID;			// indice colore utente
} STAT_SEC_STRUCT;
#define STAT_SEC_SIZE		(sizeof(STAT_SEC_STRUCT))

#define STAT_SEC_UNION_SIZE		79	// dimensione massima: 79 byte + 1 byte checksum
typedef union __packed
{
	unsigned char b[STAT_SEC_UNION_SIZE];
	STAT_SEC_STRUCT StatSec;			// struttura dati non volatili
}STAT_SEC_UNION;

typedef struct __packed
{
	STAT_SEC_UNION uStatSec;			// struttura dati non volatili
	unsigned char Checksum;				// checksum di controllo della struttura
} NVDATA_STRUCT;
#define NVDATA_SIZE		(sizeof(NVDATA_STRUCT))

#define NVRAM_SIZE	(20)		// dimensione RAM non volatile in 32-bit word
typedef union __packed
{
	unsigned long Array[NVRAM_SIZE];
	NVDATA_STRUCT NVData;
} NVRAMDATA;
extern NVRAMDATA NVRam;

#define StatSec							NVRam.NVData.uStatSec.StatSec
#define NVChecksum						NVRam.NVData.Checksum

#define Crono							(StatSec.CronoPar)

enum
{
	UCOL_BLUE,
	UCOL_GREEN,
	UCOL_YELLOW,
	UCOL_ORANGE,
	UCOL_SCARLET,
	UCOL_PURPLE,
	UCOL_PINK,
	UCOL_GREY,
	UCOL_BLACK,
	UCOL_MAX,
};
#define UserColorId						(StatSec.UserColorID)

#define PAN_BYTE_SETS					StatSec.PanSet.BYTE_SETS
#define ENABLE_BOOT						StatSec.PanSet.BIT_SETS.EnBoot
#define F_TONI_ON_PAN					StatSec.PanSet.BIT_SETS.unEnToni
#define STATO_CRONO						StatSec.PanSet.BIT_SETS.unStatoCrono
#define VIS_SANITARI						StatSec.PanSet.BIT_SETS.VisSanitari
#define VIS_TIPOSTUFA					StatSec.PanSet.BIT_SETS.VisTipoStufa
#define SHOWTEMP							StatSec.PanSet.BIT_SETS.ShowTemp
enum
{
	SHOW_TEMP_AUTO,
	SHOW_TEMP_CELSIUS,
	SHOW_TEMP_FAHRENHEIT,
	NSHOWTEMP
};

#define RFid	(StatSec.RFidPar)

#define RFID_MIN			0
#define RFID_MAX			254
#define RFID_DEF			RFID_MIN
#define RFID_JOLLY		255		// ID jolly

#define LANG_DEF		255	// codice per lingua al default




/* prototipi */
short nvramInit( void );
short nvramUpdate( void );

void CronoDef( void );
void CfgDef( void );
void CronoDef( void );
void RecConfig( void );

void ScriviCrono(BYTE ByValue);
void ScriviCronoProfile(BYTE ByValue);
void ScriviCronoProgWeekStart( BYTE index, BYTE val );
void ScriviCronoProgWeekStop( BYTE index, BYTE val );
void ScriviCronoProgWeekAbil( BYTE index, BYTE val );
void ScriviCronoProgWeekTempAria( BYTE index, BYTE val );
void ScriviCronoProgWeekTempH2O( BYTE index, BYTE val );
void ScriviCronoProgWeekFire( BYTE index, BYTE val );

void ScriviRFid( BYTE ByValue );


#endif	// _NVRAM_H_


