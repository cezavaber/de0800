/*!
** \file TimeBase.h
** \author Alessandro Vagniluca
** \date 16/10/2015
** \brief Header file for TIMEBASE.C source module
** 
** \version 1.0
**/

#ifndef __TIMEBASE_H
#define __TIMEBASE_H

#ifdef __cplusplus
 extern "C" {
#endif


/*!
** Timer state defines
**/
#define NON_ATTIVO	0
#define INCORSO		1
#define SCADUTO		2
#define NOTIMER		0xFFFFFFFFL

/*!
** Timer value defines
**/
#define T1CEN       10		//!< 10ms with tick=1ms
#define T1DEC       100		//!< 100ms with tick=1ms
#define T1SEC       1000	//!< 1s with tick=1ms
#define T1MIN       60000	//!< 1min with tick=1ms

/*!
** \enum Timers_enum
** \brief Software timers' enumeration
** \note Max 32 timers with 1 tick resolution
**/
enum Timers_enum
{
	TRUN,
	TLED,
	THOST,
	T_ADC,
	T_PULS,
	
	NTIMER
};
#if NTIMER > 32
	#error ERROR: too many software timers are defined
#endif

/*!
** \fn void starttimer( unsigned char quale, unsigned long attesa )
** \brief Start a software timer
** \param quale The timer index
** \param attesa The timer value (ticks)
**/
void starttimer( unsigned char quale, unsigned long attesa );

/*!
** \fn unsigned char checktimer( unsigned char quale )
** \brief Check a software timer
** \param quale The timer index
** \return The timer state:
** 			SCADUTO - elapsed
** 			INCORSO - not elapsed yet
** 			NON_ATTIVO - disabled
** \note When SCADUTO is returned, the timer is automatically disabled
**/
unsigned char checktimer( unsigned char quale );

/*!
** \fn unsigned long readtimer( unsigned char quale )
** \brief Read the current value of a software timer
** \param quale The timer index
** \return The timer value (ticks) or NOTIMER if disabled
**/
unsigned long readtimer( unsigned char quale );


#ifdef __cplusplus
}
#endif

#endif	// __TIMEBASE_H
