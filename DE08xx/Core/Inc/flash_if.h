/*!
** \file flash_if.h
** \author Alessandro Vagniluca
** \date 07/01/2017
** \brief FLASH_IF.C header file
** 
** \version 1.0
**/

#ifndef __FLASH_IF_H
#define __FLASH_IF_H

#ifdef __cplusplus
extern "C" {
#endif


/*!
** \fn void Flash_If_Init( void )
** \brief Initialize Flash interface (unlock + sprotect)
** \return None
**/
void Flash_If_Init( void );

/*!
** \fn void Flash_If_Deinit( void )
** \brief End Flash interface (lock + protect)
** \return None
**/
void Flash_If_Deinit( void );

/*!
** \fn HAL_StatusTypeDef Flash_If_Erase_Sector( unsigned long Sector )
** \brief Erase the specified FLASH memory sector
** \param Sector FLASH sector to erase
** \return HAL_OK: OK, otherwise: FAILED
** \note This function defines the word (32-bit) erase parallelism
** 		(the device voltage range is supposed to be from 2.7V to 3.6V).
**/
HAL_StatusTypeDef Flash_If_Erase_Sector( unsigned long Sector );

/*!
** \fn HAL_StatusTypeDef Flash_If_Program_WordBuffer( unsigned long Address,
** 													unsigned long *DataBuf,
** 													unsigned long size )
** \brief Program a word (32-bit) buffer at a specified address
** \param Address The start address to be programmed
** \param DataBuf The data buffer to be programmed
** \param size    The data word number
** \return HAL_OK: OK, otherwise: FAILED
** \note This function must be used when the device voltage range is from 2.7V to 3.6V.
**/
HAL_StatusTypeDef Flash_If_Program_WordBuffer( unsigned long Address,
												unsigned long *DataBuf,
												unsigned long size );


#ifdef __cplusplus
}
#endif

#endif	// __FLASH_IF_H
