/*!
** \file Loader.h
** \author Alessandro Vagniluca
** \date 15/12/2016
** \brief LOADER.C header file
** 
** \version 1.0
**/

#ifndef __LOADER_H
#define __LOADER_H

#include "main.h"

/*--------------------------------------------- STM32F411xC/E -------------------------------------------*/ 
/* Base address of the Flash sectors Bank 1 */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_END		((uint32_t)0x08080000)

/* Base address of the SRAM */
#define SRAM_START_ADDRESS		(SRAM1_BASE)
#define SRAM_SIZE				(0x20000L)
/*-----------------------------------------------------------------------------------------------------*/

/*!
** \def ADDR_FIRMAAPPL
** \brief The start address of the application mark
**/
#define ADDR_FIRMAAPPL			(ADDR_FLASH_END - FIRMA_LEN)

/*!
** \def ADDR_APPL
** \brief The start address of the application
**/
#define ADDR_APPL				(ADDR_FLASH_SECTOR_4)


#ifdef __cplusplus
extern "C" {
#endif


enum
{
	MEM_ROMCODE,	//!< flash code
	MEM_ROMDATA,	//!< flash data
	//MEM_EEDATA1,		//!< eeprom 1 data
	//MEM_EEDATA2,		//!< eeprom 2 data
	MEM_DFDATA1,		//!< dataflash 1 data
	//MEM_DFDATA2,		//!< dataflash 2 data
	NMEM
};


#define MAXDATASIZE			(256)	// max number of data+checksum bytes in a S-record
										// must be multiple of sizeof(long)

/*!
** \var Data
** \brief The data buffer
**/
typedef union
{
	unsigned char DataByte[MAXDATASIZE];
	unsigned long DataDWord[MAXDATASIZE/sizeof(long)];
} DATASTRUCT;
extern DATASTRUCT *Data;


/*!
** \fn short WriteMem( unsigned char TargetMemory, unsigned long Addr,
** 						DATASTRUCT *Data, unsigned long size )
** \brief Write data to the target memory
** \param TargetMemory The target memory
** \param Addr The start address
** \param Data The data buffer
** \param size The number of bytes to write
** \return	0: OK
** 			-1: invalid target memory
** 			-2: verify error
** 			-3: blank-check error
**/
short WriteMem( unsigned char TargetMemory, unsigned long Addr,
				DATASTRUCT *Data, unsigned long size );

/*!
** \fn short loader_Init( unsigned char TargetMemory )
** \brief Initialize the loader on the target memory
** \param TargetMemory The target memory
** \return 1: OK, 0: FAILED
**/
short loader_Init( unsigned char TargetMemory );

/*!
** \fn short loader_DeInit( unsigned char TargetMemory )
** \brief DeInitialize the loader on the target memory
** \param TargetMemory The target memory
** \return 1: OK, 0: FAILED
**/
short loader_DeInit( unsigned char TargetMemory );

/*!
** \fn short loader( unsigned char TargetMemory, const char *SRecord )
** \brief Load S-record data in the target memory
** \param TargetMemory The target memory
** \param SRecord The S-record string
** \return	1: end loading 
** 			0: record OK
** 			-1: invalid S-record
** 			-2: verify error
** 			-3: blank-check error
** 			-4: abort
** 			-5: erase/ID
**/
short loader( unsigned char TargetMemory, const char *SRecord );


#ifdef __cplusplus
}
#endif

#endif	// __LOADER_H
